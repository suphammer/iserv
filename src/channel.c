/*
 * channel.c - channel database system
 * (C) 1996-2000 by Jon Suphammer
 *
 */ 

#include "global.h"
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>

extern channelsclass channels;
extern nicksclass nicks;


sChannelList *ctop;
sUserList *utop;
sUserList *tmpvar;
long LASTBACKUP;

// ---------------------------------------------
// Function: add_channel
// Desc.:    Legger til en kanal
// ---------------------------------------------
sChannelList* add_channel(sChannelList *p, sChannelList u)
{
  if (p == NULL)
    {
      p = (sChannelList *) malloc(sizeof(sChannelList));
      memcpy(p,&u,sizeof(struct TagChannelList));
      p->next = NULL;
    }
  else p->next=add_channel(p->next,u);
  return p;
}

void cmd_add_channel(char *channel)
{
  int i;
  sChannelList clist;
  strcpy(clist.name,channel);
  strcpy(clist.flags,"+n +t");
  strcpy(clist.topic,"");
  clist.ulist=NULL;
  clist.strictt='\0';
  clist.created=time(NULL);
  for(i=0; i!=5; i++)
     strcpy(clist.onjoin[i],"");
  ctop=add_channel(ctop,clist);
  // show_channels();
}


/* ----------------------------------------------
 * Function: validate channel name
 * Desc:     Validates that channel name is legal
 * Author:   Jon Thomas Stokkeland aka Stoke 2004
 */
int validate_channel_name (char *channel)
{
   if (*channel != '#') return False;  // Must start with #
   if (strlen(channel) > 50) return False;;  // Max length according to RFC 2811
   channel++;
   for (; *channel != '\0' ;channel++)
   {
     if (( (*channel <= 'z') && (*channel >= 'A')) || ((*channel <= '9') && (*channel >= '0'))) continue;
     else
     {
	// We only allow channel names with basic characters and symbols to be registered
	switch (*channel)
	{
	   case '-':
	   case '_':
	   case '.':
	     continue;
	   default:
	     return False;
	}

     }
   }
   return True;
}


// ---------------------------------------------
// Function: show_channels
// Desc.:    Lister ut alle kanalene
// ---------------------------------------------

void show_channels_(sChannelList *t)
{
  if (t != NULL)
    {
      printf("ChannelList: %s, %s\n", t->name, t->flags);
      show_channels_(t->next); 
    }
}

void show_channels()
{
  show_channels_(ctop);
}

void join_channels_(sChannelList *t)
{
  if (t != NULL)
    {
      send_join(t);
      join_channels_(t->next); 
    }
}

void join_channels()
{
  join_channels_(ctop);
}

void op_channels_(sChannelList *t)
{
  if (t != NULL)
    {
      send_op(t);
      op_channels_(t->next);
    }
}

void op_channels()
{
  op_channels_(ctop);
}

// ---------------------------------------------
// Function: find_channel
// Desc.:    Finner en kanal og returnerer peker
// ---------------------------------------------
sChannelList *find_channel(char *name)
{
  sChannelList *current=NULL;
  sChannelList *found=NULL;
  
  for (current=ctop; current!=NULL; current=current->next)
    if ( strcasecmp(current->name,name)==0 ) found=current;
  return found;
}

// ---------------------------------------------
// Function: find_user
// Desc.:    Finner en bruker p� en kanal og returnerer peker
// ---------------------------------------------
/*
sUserList *find_user(char *name, char *nick)
{
  sChannelList *tmpname;
  sUserList *current=NULL;
  sUserList *found=NULL;

  if(tmpname=find_channel(name)) {
    for (current=tmpname->ulist; current!=NULL; current=current->next)
      if ( strcasecmp(current->nick,nick)==0 ) found=current;
    return found;
  }
  else
    return NULL;
}
*/


// ---------------------------------------------
// Function: find_user_from_host
// Desc.:    Finner en bruker p� en kanal og returnerer peker
// ---------------------------------------------
sUserList *find_user_from_host(char *name, char *host)
{
  sChannelList *tmpname;
  sUserList *current=NULL;
  sUserList *found=NULL;

  if(tmpname=find_channel(name)) {
    for (current=tmpname->ulist; current!=NULL; current=current->next)
      if ( !match(current->mask,host) ) found=current;
    return found;
  }
  else
    return NULL;
}

// ---------------------------------------------
// Function: show_users
// Desc.:    Vis liste over brukerene i en liste
// ---------------------------------------------
void show_users_(char *to,sUserList *t)
{
  char templine[255];
  if (t != NULL)
    {
      sprintf(templine,"%-10s %3s  %s\n",
	      t->nick, t->flags, ctime(&t->seen));
      send_to_user(to,templine);
      show_users_(to, t->next); 
    }
}

void show_users(char *to, char *name)
{
  sChannelList *tmpname;
  char templine[255];
  if(tmpname=find_channel(name)) {
    if(tmpname->ulist==NULL) send_to_user(to,"No users!");
    sprintf(templine,"Userlist for channel %s:",tmpname->name); send_to_user(to,templine);
    sprintf(templine,"%cName       AOP  Last seen               %c",22,22);
    send_to_user(to,templine);
    show_users_(to, tmpname->ulist);
  }
  else {
    sprintf(templine,"Channel '%s' not found!",name);
    send_to_user(to,templine);	// printf("Channel not found!\n");
  }
  //send_to_user(to,"");
}

void show_users_all_(char *to,sUserList *t)
{
  char templine[255];
  if (t != NULL)
    {
      sprintf(templine,"%-10s %3d %3s  %s\n",
	      t->nick, t->level, t->flags, ctime(&t->seen));
      send_to_user(to,templine);
      show_users_all_(to, t->next); 
    }
}

void show_users_all(char *to, char *name)
{
  sChannelList *tmpname;
  char templine[255];
  if(tmpname=find_channel(name)) {
    if(tmpname->ulist==NULL) send_to_user(to,"No users!");
    sprintf(templine,"Userlist for channel %s:",tmpname->name); send_to_user(to,templine);
    sprintf(templine,"%cName    Ulevel AOP  Last seen               %c",22,22);
    send_to_user(to,templine);
    show_users_all_(to, tmpname->ulist);
  }
  else {
    sprintf(templine,"Channel '%s' not found!",name);
    send_to_user(to,templine);	// printf("Channel not found!\n");
  }
  //send_to_user(to,"");
}

// ---------------------------------------------
// Function: add_user
// Desc.:    Legger til en bruker
// ---------------------------------------------
sUserList* add_user(sUserList *p, sUserList u)
{
  if (p == NULL)
    {
      p = (sUserList *) malloc(sizeof(sUserList));
      memcpy(p,&u,sizeof(struct TagUserList));
      // p->authed = 0;
      p->next = NULL;
    }
  else p->next=add_user(p->next,u);
  return p;
}

/*
sUserList* cmd_add_user(char *channel, char *nick, char *mask, int *level, long *authtime)
{
  sUserList ulist,*tmpuser;
  sChannelList *tmpname;

  if(tmpname=find_channel(channel))
    if(tmpuser=find_user(channel,nick)) {
      strcpy(tmpuser->nick,nick);
      tmpuser->level=level;
      strcpy(tmpuser->flags,"0");
      tmpuser->seen=time(NULL);
      strcpy(tmpuser->passwd,"");
      tmpuser->authed=authtime;
      strcpy(tmpuser->comment,"");
      strcpy(tmpuser->mask,mask);
      strcpy(tmpuser->authchan,"");
    }
    else {
      strcpy(ulist.nick,nick);
      ulist.level=level;
      strcpy(ulist.flags,"0");
      ulist.seen=0;
      strcpy(ulist.passwd,"");
      ulist.authed=authtime;
      strcpy(ulist.comment,"");
      strcpy(ulist.mask,mask);
      if(tmpname->ulist!=NULL) {
	utop=tmpname->ulist;
	utop=add_user(utop,ulist);
      }
      else {
	utop = (sUserList *) malloc(sizeof(sUserList));
	memcpy(utop,&ulist,sizeof(struct TagUserList));
	utop->next = NULL;
	tmpname->ulist=utop;
      }
    }
}
*/

// ---------------------------------------------
// Function: rem_user
// Desc.:    Slette en bruker
// ---------------------------------------------

int rem_user(char *channel, char *nick)
{
  sChannelList *tmpchan;
  sUserList *current=NULL, *prev=NULL, *tmpuser=NULL, *candidate;

  if(!(tmpchan=find_channel(channel))) return FALSE;

  for (current=tmpchan->ulist; current!=NULL; current=current->next) {
    if (strcasecmp(current->nick,nick)==0 ) tmpuser=current;
    if (tmpuser==NULL) prev=current;
    }

  if(tmpuser!=NULL) {
    //printf("Removing user\n");
    //if(prev!=NULL) printf("prev->nick: %s\n",prev->nick);
    //else printf("prev->nick: N/A\n");
    //printf("current->nick: %s\n",tmpuser->nick);
    if(prev!=NULL) {
      candidate = prev->next;
      prev->next=tmpuser->next;
    }
    else {
      candidate = tmpuser;
      tmpchan->ulist=tmpuser->next;
    }
    free(candidate);
    return TRUE;    
  }
  return FALSE;
}

// ---------------------------------------------
// Function: rem_chan
// Desc.:    Slette en kanal
// ---------------------------------------------

int rem_chan(char *channel)
{

  sChannelList *current=NULL, *prev=NULL, *tmpchan=NULL, *candidate;

  for (current=ctop; current!=NULL; current=current->next) {
    if ( strcasecmp(current->name,channel)==0 ) tmpchan=current;
    if (tmpchan==NULL) prev=current;
  }

  if(tmpchan!=NULL) {
    //printf("Removing channel\n");
    //if(prev!=NULL) printf("prev->name: %s\n",prev->name);
    //else printf("prev->name: N/A\n");
    //printf("current->name: %s\n",tmpchan->name);
    if(prev!=NULL) {
      candidate = prev->next;
      prev->next=tmpchan->next;
    }
    else {
      candidate = tmpchan;
    }
    free(candidate);
    return TRUE;    
  }
  return FALSE;
}

// ---------------------------------------------
// Function: load_channels
// Desc.:    Laster inn data fra w.channels
// ---------------------------------------------
void load_channels()
{
  FILE *fp;
  char line[255], *cmd, *rest;
  int i,oi;
  sChannelList clist;
  sUserList ulist;
  int chan_users;
  ctop=NULL;
  chan_users=0;


  fp = fopen((char *)&CHANFILE,"r");
  for (i=0;fgets(line, 255, fp);i++) {
    if(line[0]!='#') {
      if(( rest = index( line, ' ' )) !=0)
	*( rest ) = '\0'; rest += 1;
	cmd = line;
	*( rest + strlen(rest) -1 ) ='\0';

	if(cmd!=NULL && rest!=NULL) {
	  // printf("CMD: %s, REST: %s\n",cmd, rest);

	  if(!strcmp(cmd,"CNAME")) {
         // putlog("Channel: %s\n",rest);
	  		strcpy(clist.name,rest);
             for(oi=0; oi!=5; oi++)
                strcpy(clist.onjoin[oi],"");
     }

	  if(!strcmp(cmd,"CFLAGS")) {
	    strcpy(clist.flags,rest);
	  }
	  if(!strcmp(cmd,"CSTRICTT")) {
	    clist.strictt=atoi(rest);
	  }
	//clist.created=(long *)time(NULL)-2678400;
	  if(!strcmp(cmd,"CCREATED")) {
	    clist.created=atol(rest);
	  }
          if(!strcmp(cmd,"ONJOIN1"))
            strcpy(clist.onjoin[0],rest);
          if(!strcmp(cmd,"ONJOIN2")) 
            strcpy(clist.onjoin[1],rest);
          if(!strcmp(cmd,"ONJOIN3")) 
            strcpy(clist.onjoin[2],rest);
          if(!strcmp(cmd,"ONJOIN4")) 
            strcpy(clist.onjoin[3],rest);
          if(!strcmp(cmd,"ONJOIN5")) 
            strcpy(clist.onjoin[4],rest);

	  if(!strcmp(cmd,"CTOPIC")) {
	    strcpy(clist.topic,rest);
	    chan_users=FALSE;
        #ifdef DEBUG
	    printf("%s\n",clist.name);
        #endif
	  }
	  
	  // Advarsel; Kanal uten brukere, allokeres ikke!
			 
	    if(!strcmp(cmd,"UNICK")) {
	      strcpy(ulist.nick,rest);
	    }
	  
	    if(!strcmp(cmd,"ULEVEL")) {
	      ulist.level=atoi(rest);
	    }

	    if(!strcmp(cmd,"UFLAGS")) {
	      strcpy(ulist.flags,rest);
	    }

	    if(!strcmp(cmd,"USEEN")) 
	      ulist.seen=atol(rest);
	    }

	    if(!strcmp(cmd,"UPASSWD")) {
	      strcpy(ulist.passwd,rest);
	    }

	    if(!strcmp(cmd,"UCOM")) {
	      strcpy(ulist.comment,rest);
	    }

	    if(!strcmp(cmd,"UMASK")) {
	      strcpy(ulist.mask,rest);
	    }
	   
            if(!strcmp(cmd,"AUTHCHAN")) {
              strcpy(ulist.authchan,rest);
            }

	    if(!strcmp(cmd,"UEND")) {
	      if(chan_users==FALSE) {
		chan_users=TRUE;
		clist.ulist=add_user(NULL,ulist);
		ctop=add_channel(ctop,clist);
		tmpvar=ctop->ulist;
		#ifdef DEBUG
		printf(" %s\n",ctop->ulist->nick);
		#endif
		utop=clist.ulist;
	      }
	      else {
		utop=add_user(utop,ulist);
		#ifdef DEBUG
                printf("  %s\n",ulist.nick);
		#endif
	      }	    
	    }
    }
  }
  fclose(fp);
}

// ---------------------------------------------
// Function: save_channels_sql
// Desc.:    Writes cache data to sql
// ---------------------------------------------

void save_channels_sql()
{
  char line[255], *cmd, *rest;
  sChannelList *channel=NULL;
  sUserList *user=NULL;
  time_t tloc;
  int oi;
  //nicksclass nicks;
  //channelsclass channels;
  sChannelList *mychannel,mychannelptr;
  sUserList *myuser,myuserptr;
  mychannel = &mychannelptr;
  myuser = &myuserptr;

  //nicks.open();
  //channels.open();

  for (channel=ctop; channel!=NULL; channel=channel->next)
  {
    if(channel->name[0]=='#')
    {
      
      if(!channels.findChannel(channel->name,mychannel))
      {
        channels.clearProperties();

        if(channel->name[0]!=0)
          strncpy(channels.channel.name,channel->name,CHANNELLEN);
        if(channel->flags[0]!=0)
          strncpy(channels.channel.flags,channel->flags,UFLAGSLEN);
        channels.channel.strictt = channel->strictt;
        channels.channel.created = channel->created;
        for(oi=0; oi!=5; oi++)
          if(strcmp(channel->onjoin[oi],""))
            strncpy(channels.channel.onjoin[oi],channel->onjoin[oi],ONJOINLEN);
        if(channel->topic[0]!=0)
          strncpy(channels.channel.topic,channel->topic,TOPICLEN);

        if(channels.addChannelExport(channel->name,""))
        {
          //printf("Channel: %s - sucessfully added\n",channel->name);
        }
        else
        {
          printf("Channel: %s - error adding\n",channel->name);
        }
      }
      else
        printf("Channel: %s - allready exists\n",channel->name);

      // Usertable

      for (user=channel->ulist; user!=NULL; user=user->next) {

        if(!nicks.findUser(channel->name,user->nick,myuser))
        {
          nicks.clearProperties();

          nicks.user.level = user->level;
          if(user->flags[0]!=0)
            strncpy(nicks.user.flags,user->flags,UFLAGSLEN);
          nicks.user.seen = user->seen;
          if(user->passwd[0]!=0)
            strncpy(nicks.user.passwd,user->passwd,PASSWDLEN);
          if(user->comment[0]!=0)
            strncpy(nicks.user.comment,user->comment,COMMENTLEN);
          if(user->mask[0]!=0)
            strncpy(nicks.user.mask,user->mask,MASKLEN);
          if(user->authchan[0]!=0)
            strncpy(nicks.user.authchan,user->authchan,CHANNELLEN);
          if(nicks.addUserExport(channel->name,user->nick))
          {
            //printf("Nick: %s - sucessfully to %s\n",user->nick,channel->name);
          }
          else
          {
            printf("Nick: %s - error adding into %s\n",user->nick,channel->name);
          }
        }
        else
          printf("Nick: %s - allready exists in %s\n",user->nick,channel->name);
      }
    }
  }
}


// ---------------------------------------------
// Function: ipmask
// Desc.:    Makes a mask of a user@host address
// ---------------------------------------------

void ipmask(char *orgmask, char *newmask)
{
  // char newmask[32];
  char tegn[2];
  int ftall,orgt,newt,checkt,gothost;
  tegn[1]='\0';
  ftall=FALSE;

  for (orgt=0, newt=0; (orgmask[orgt]!='@' && orgmask[orgt]!='\0'); orgt++) {
    if(orgmask[orgt]!='~')
      { newmask[newt]=orgmask[orgt]; newt++;}
  }

  newmask[newt]=orgmask[orgt];newt++;orgt++;

  for (checkt=orgt,gothost=FALSE; checkt<strlen(orgmask); checkt++) {
    tegn[0]=orgmask[checkt];tegn[1]='\0';
    if((atoi(tegn)==0) && (tegn[0]!='.') && (tegn[0]!='0')) gothost=TRUE;
  }

  if(gothost==TRUE) {
    for (; orgt<strlen(orgmask); orgt++) {
      tegn[0]=orgmask[orgt];tegn[1]='\0';
      if((atoi(tegn)>0) || tegn[0]=='0') {
	if(ftall==FALSE) {
	  newmask[newt]='*';
	  newt++;
	  ftall=TRUE;
	}
      }
      else {
	newmask[newt]=tegn[0];
	newt++;
	ftall=FALSE;
      }
    }
  }
  else {
    for (checkt=0; orgt<strlen(orgmask); orgt++) {
      if(orgmask[orgt]=='.') checkt++;
      if(checkt<=3) { newmask[newt]=orgmask[orgt];newt++;}
      if(checkt==3) { newmask[newt]='*';newt++;checkt++;}
    }
  }
  newmask[newt]='\0';
  // printf("Newmask: %s\n",_newmask);
}

// ---------------------------------------------
// Function: banmask
// Desc.:    Makes a mask of a user@host address
// ---------------------------------------------
void banmask(char *orgmask, char *newmask)
{
  // char newmask[32];
  char tegn[2];
  int ftall,orgt,newt,checkt,gothost;
  tegn[1]='\0';
  ftall=FALSE;

  // Will fail if orgmask does not have a @
  for (orgt=0, newt=0; orgmask[orgt]!='@'; orgt++) {
    if(orgmask[orgt]!='~') {
	newmask[newt]=orgmask[orgt]; 
        newt++;
    }
    // newmask[newt]=orgmask[orgt]; 
  }

  newmask[newt]=orgmask[orgt];newt++;orgt++;

  for (checkt=orgt,gothost=FALSE; checkt<strlen(orgmask); checkt++) {
    tegn[0]=orgmask[checkt];tegn[1]='\0';
    if((atoi(tegn)==0) && (tegn[0]!='.') && (tegn[0]!='0')) gothost=TRUE;
  }

  if(gothost==TRUE) {
    for (; orgt<strlen(orgmask); orgt++) {
      tegn[0]=orgmask[orgt];tegn[1]='\0';
      if((atoi(tegn)>0) || tegn[0]=='0') {
        if(ftall==FALSE) {
          newmask[newt]='*';
          newt++;
          ftall=TRUE;
        }
      }
      else {
        newmask[newt]=tegn[0];
        newt++;
        ftall=FALSE;
      }
    }
  }
  else {
    for (checkt=0; orgt<strlen(orgmask); orgt++) {
      if(orgmask[orgt]=='.') checkt++;
      if(checkt<=3) { newmask[newt]=orgmask[orgt];newt++;}
      if(checkt==3) { newmask[newt]='*';newt++;checkt++;}
    }
  }
  newmask[newt]='\0';
  putlog ("Newmask: %s\n",newmask);
}

