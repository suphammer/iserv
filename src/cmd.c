/*
 * cmd.c - channel services functions
 * (C) 1996-2000 by Jon Suphammer
 * 
 * edits by J.Thomas Stokkeland (Stoke) 2004
 * $Id: cmd.c 4 2004-11-09 06:48:12Z stoke $
 */ 

#include "global.h"
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
extern char buf[512];
extern char fromhost[HOSTLEN];
extern channelsclass channels;
extern nicksclass nicks;

struct
{
	char      *name;		// name of command
	void      (*function)(void);	// pointer to function
	int       ulevel;		// required userlvl
	int       global;
} on_msg_commands[] =
//	Command		function   userlvl   global
{
	{ "QUIT",	do_quit,	700, TRUE  },
	{ "RAW",	do_raw,		900, TRUE  },
	{ "SAVECACHE",	do_savecache,	800, TRUE  },
	{ "DO",		do_do,		800, TRUE  },
	{ "SERVER",	do_server,	800, TRUE  },
	{ "BROADCAST",	do_broadcast,	800, TRUE  },
	{ "WHO",	do_who,		800, TRUE  },
	{ "ADDCHAN",	do_addchan,	650, TRUE  }, 
	{ "REMCHAN",	do_remchan,	650, TRUE  }, 
	{ "RESYNC",	do_resync,	650, TRUE  },
	{ "MODE",	do_mode,	650, TRUE  },
	{ "PART",	do_part,	600, TRUE  },
	{ "CLEANCHAN",	do_cleanchan,	600, FALSE },
	{ "JOIN",       do_join,        500, TRUE  },
	{ "SAY",	do_say,		500, FALSE },
	{ "ACT",	do_act,		500, FALSE },
	{ "SETLEVEL",	do_setlevel,	490, FALSE },
	{ "ONJOIN",	do_onjoin,	490, FALSE },
	{ "ADDUSER",	do_adduser,	450, FALSE },
	{ "REMUSER",	do_remuser,	450, FALSE },
	{ "TOPIC",	do_topic,	300, FALSE },
	{ "KEEPTOPIC",	do_keeptopic,	300, FALSE },
	{ "CHANINFO",	do_chaninfo,	300, FALSE },
	{ "KICK",	do_kick,	300, FALSE },
	{ "BAN",	do_ban,		300, FALSE },
	{ "DEOP",	do_deop,	100, FALSE },
	{ "OP",		do_op,		100, FALSE },
	{ "AUTOOP",	do_autoop,	100, FALSE },
	{ "VOICE",	do_voice,	 80, FALSE },
	{ "DEVOICE",	do_devoice,	 80, FALSE },
	{ "COMMENT",	do_comment,	 50, FALSE },
	{ "INVITEME",	do_inviteme,	 50, FALSE },
	{ "SETPASS",	do_setpass,	  1, FALSE },
	{ "DEAUTH",	do_deauth,	  1, FALSE },
	{ "AUTHCHAN",	do_authchan,	  1, FALSE },
	{ "AUTH",	do_auth,	  0, FALSE },
	{ "USERINFO",	do_userinfo,	  0, FALSE },
	{ "USERLIST",	do_userlist,	  0, FALSE },
	{ "COMMANDS",	show_commands,	  0, FALSE },
	{ "HELP",	show_help,	  0, TRUE  },
        { NULL,                 null(void(*)()) }
//        { NULL,                 null(void(*)), 0, 0     }
//	{ NULL,			null(void(*)()), 0, 0     }
};

typedef struct TagCmdDo {
	sUserList *user;
	char *channel;
	sChannelList *chanstr;
	char *param;
	int clevel;
	int cmdlevel;
} sCmdDo;

// sUserList *from	-> user
// char *to		-> channel
// char *rest		-> param

sCmdDo CmdDo;

void on_msg(char *from, char *to, char *msg)
{
	int i;
        int authed;
        int founduser;
	char msg_copy[MAXLEN];	/* for session */
	char *command;
	char *channel;
	sUserList *user, userptr, *cuser, cuserptr;
	sUserList tmpuser;
        sChannelList chanstrptr;
	sSrvUserList *srvuser;

        user=&userptr;
        cuser=&cuserptr;
        CmdDo.chanstr = &chanstrptr;

	strncpy(msg_copy, msg, MAXLEN);

	// Clearup CmdDo parameters
	CmdDo.user=NULL;
	CmdDo.channel=NULL;
	//CmdDo.chanstr=NULL;
        memset(CmdDo.chanstr, 0, sizeof(CmdDo.chanstr));
	CmdDo.param=NULL;
	CmdDo.clevel='\0';
	CmdDo.cmdlevel='\0';

        // Clearup other variables
        authed=FALSE;

	if(strlen(msg)>MAXLEN) senddebug("Flooding attempt!");

	*(msg+100)='\0';
	if((command = get_token(&msg, ",: "))== NULL)
		return;			// NULL-command
  
	if(STRCASEEQUAL((char *)&BOTNAME, command))
	{
		if((command = get_token(&msg, ",: "))==NULL)
			return;		// NULL-command
	}
	else if((*command != PREFIX_CHAR) && !STRCASEEQUAL(to, (char *)&BOTNAME))
		return; // The command should start with PREFIX_CHAR if public
  
	if(*command == PREFIX_CHAR)
		command++;
  
	senddebug("----------------------------------------");
	//sprintf(buf,"%s!%s: %s %s",from,Srvgetuserhost(from),command, msg ? msg : ""); //senddebug(buf);
	//putlog("[Debug] %s\n",buf);

	// Now we got
	// - A public command.
	// - A private command/message. 
   // Time to do a flood check :).

	// if(check_session(from) == IS_FLOODING)
	//	return;
 
	for(i = 0; on_msg_commands[i].name != NULL; i++)
		if(STRCASEEQUAL(on_msg_commands[i].name, command)) {
			if(!STRCASEEQUAL(to, (char *)&BOTNAME)) {
				sprintf(buf,">>PUBLIC<<[%s!%s]->%s: %s %s",from,Srvgetuserhost(from),to,command, msg ? msg : "");senddebug(buf);
			}
			else {
				if(msg!=NULL && msg[0]=='#') {
					to = get_token(&msg, ",: ");
					senddebug("Found channel-char");
				}
				else
					to = from;
				sprintf(buf,">>PRIVATE<<[%s!%s]->%s: %s %s",from,Srvgetuserhost(from),to ? to : "",command, msg ? msg : "");senddebug(buf);
			}

      // Ny struktur for overføring av data mellom prosedyrer
			CmdDo.user=user;
			CmdDo.channel=to;
			CmdDo.param=msg;


      // Storing request in log
      sprintf(buf,"%s!%s: %s %s",from,Srvgetuserhost(from),command, msg ? msg : ""); //senddebug(buf);
      if(!strcasecmp(command,"auth") || !strcasecmp(command,"setpass"))
         putlog("[Command] (%s!%s) %s: %s *\n",from,Srvgetuserhost(from),CmdDo.channel,command);
      else
         putlog("[Command] (%s!%s) %s: %s %s\n",from,Srvgetuserhost(from),CmdDo.channel,command, CmdDo.param ? CmdDo.param : "");

      // New parse-routine
			if(srvuser=Srvfind_user(from)) {
				// printf(" Onlinetable: %s\n",srvuser->host);

                                user=&tmpuser;
                                if(nicks.findUser(to,from,user))
                                   founduser=TRUE;
                                else
                                   founduser=FALSE;
                                if((founduser == TRUE) || (on_msg_commands[i].ulevel==0))
                                {
                                   if(founduser==FALSE)
                                   {
                                      senddebug("Level 0 command (No user found)");
                                      strncpy(user->nick,from,NICKLEN);
                                      user->level = 0;
                                      strncpy(user->mask,"*",MASKLEN);
                                   }

                                   authed=isauthed(user,srvuser,to);

                                   if(!authed)
                                   {
                                      senddebug("Level 0 command (User not authed)");
                                      strncpy(user->nick,from,NICKLEN);
                                      user->level = 0;
                                      strncpy(user->mask,"*",MASKLEN);
                                   }

                                   if((authed || !match(user->mask,srvuser->host)) || (on_msg_commands[i].ulevel==0))
                                   {
/*
                                      if(match(user->mask,srvuser->host))
                                      {
                                         senddebug("Level 0 command with no user..ip section..");
                                         strncpy(user->nick,from,NICKLEN);
                                         user->level = 0;
                                         strncpy(user->mask,"*",MASKLEN);
                                      }
*/
                                      senddebug("Bruker er godkjent!");
                                      if((!authed) && (on_msg_commands[i].ulevel>0))
                                      {
                                         strncpy(user->nick,from,NICKLEN);
                                         user->level = 0;
                                         strncpy(user->mask,"*",MASKLEN);
                                      }

                                      if((user->level) >= on_msg_commands[i].ulevel)
                                      {
                                         senddebug("Userlevel ok.");
                                         if(((nicks.findUser((char *)&CSERVICE,from,cuser)) &&
                                          (!match(cuser->mask,srvuser->host)) &&
                                          (cuser->level >= on_msg_commands[i].ulevel))
                                          && (cuser->authed==srvuser->logon))
                                            CmdDo.clevel=cuser->level;
                                         else
                                            CmdDo.clevel=0;
                                            CmdDo.cmdlevel=on_msg_commands[i].ulevel;
                                            CmdDo.user=user;
                                            CmdDo.channel=to;		

                                            channels.findChannel(CmdDo.channel,CmdDo.chanstr);

                                            CmdDo.param=msg;
                                            on_msg_commands[i].function();
                                      }
                                      else
                                      {
							if(((nicks.findUser((char *)&CSERVICE,from,user)) &&
								(!match(user->mask,srvuser->host)) &&
								(user->level >= on_msg_commands[i].ulevel)))
								if((user->authed==srvuser->logon)) {
									send_to_user(from,"Userlevel too low. Executing with Channel Service level!");
									CmdDo.user=user;
									CmdDo.channel=to;
                                                                        channels.findChannel(CmdDo.channel,CmdDo.chanstr);
									CmdDo.param=msg;
									on_msg_commands[i].function();
								}
								else {
									send_to_user(from,"Userlevel too low");
								}
						}
	    			}
					else {
						if(((nicks.findUser((char *)&CSERVICE,from,user)) &&
							(!match(user->mask,srvuser->host)) &&
							(user->level >= on_msg_commands[i].ulevel))
							&& (user->authed==srvuser->logon)) {
								send_to_user(from,"No access in current channel. Executing with Channel Service level!");
								CmdDo.user=user;
								CmdDo.channel=to;
                                                                channels.findChannel(CmdDo.channel,CmdDo.chanstr);
								CmdDo.param=msg;
								on_msg_commands[i].function();
						}
						else {
							send_to_user(from,"You are not registred in that channel");
							senddebug("That nick is registred but ip does not mach");
						}
					}
				}
				else {
					if(((nicks.findUser((char *)&CSERVICE,from,user)) &&
						(!match(user->mask,srvuser->host)) &&
						(user->level >= on_msg_commands[i].ulevel))
						&& (user->authed==srvuser->logon)) {
							send_to_user(from,"No access in current channel. Executing with Channel Service level!");
							CmdDo.user=user;
							CmdDo.channel=to;
                                                        channels.findChannel(CmdDo.channel,CmdDo.chanstr);
							CmdDo.param=msg;
							on_msg_commands[i].function();
					}
					else {
						send_to_user(from,"You are not registred in that channel");
						senddebug("That nick is not registred in that channel.");
					}
				}
			}
			else senddebug("User is not registred on IRC. Something strange is going on.");

			senddebug("End of parse-routine...");
			return;
	}
  
	senddebug("Unknown CMD!");

	// If the command was private, let the user know how stupid (s)he is
	if(STRCASEEQUAL(to, (char *)&BOTNAME))
	send_to_user(from,"%s %s? I don't understand that!",command, msg ? msg : "");
}

int isauthed(sUserList *user, sSrvUserList *srvuser, char *channel)
{
   // authchan user
   sUserList *auser, auserptr;
   // channel service user
   sUserList *cuser, cuserptr;

   auser = &auserptr;
   cuser = &cuserptr;

/*         send_to_user(user->nick,"-----------------------------");
         send_to_user(user->nick,"Current auth information:");
         send_to_user(user->nick,"Authchan: %s",user->authchan);
         send_to_user(user->nick," Channel: %s",channel);
         send_to_user(user->nick,"  Authed: %d",user->authed);
         send_to_user(user->nick,"   Logon: %d",srvuser->logon);
         send_to_user(user->nick,"Authmask: %s",user->mask);
         send_to_user(user->nick,"Yourmask: %s",srvuser->host);
         if(match(user->mask,srvuser->host)==0)
            send_to_user(user->nick,"Mask is correct");
         else
            send_to_user(user->nick,"Mask is incorrect");
*/
         if(user->authed==srvuser->logon) {
            if(match(user->mask,srvuser->host)==0) {
               //send_to_user(user->nick,"\026 You are authed by CURRENT channel! \026"); 
               return TRUE;
            }
         }

         if(strlen(user->authchan)>0)
         {
            if(strcasecmp(user->authchan,channel))
            {
               if(nicks.findUser(user->authchan,user->nick,auser))
               {
                  if(auser->authed==srvuser->logon)
                  {
                     if(match(auser->mask,srvuser->host)==0)
                     {
                        //send_to_user(user->nick,"\026 You are authed by AUTHCHAN channel! \026");
                        return TRUE;
                     }
                  }
               }
            }
         }
         //send_to_user(user->nick,"\026 You are NOT authed! \026");
         return FALSE;
}

char *get_token(char **src, char *token_sep)
/*
 * Just a little more convenient than strtok()
 * This function returns a pointer to the first token
 * in src, and makes src point to the "new string".
 *
 */
{
  char	*tok;

  if(!(src && *src && **src))
    return NULL;

  /* first, removes leading token_sep's */
  while(**src && strchr(token_sep, **src))
    (*src)++;
	
  /* first non token_sep */
  if(**src)
    tok = *src;
  else
    return NULL;
  
  /* Make *src point after token */
  *src = strpbrk(*src, token_sep);
  if(*src)
    {
      **src = '\0';
      (*src)++;
      while(**src && strchr(token_sep, **src))
	(*src)++;
    }
  else
    *src = "";
  return tok;
}


void do_quit()
{
	sprintf(buf,":%s QUIT :%s (%s)\n",&BOTNAME,CmdDo.param?CmdDo.param:"",CmdDo.user->nick);writeln(buf);
  sleep(1);
  sprintf(buf,"Quit requested by %s",CmdDo.user->nick);
  cleanup(buf);
  exit(-1);
}

void do_do()
{
  if(CmdDo.param[0])
    { sprintf(buf,":%s %s\n",&BOTNAME,CmdDo.param);writeln(buf); }
  else
    { sprintf(buf,":%s PRIVMSG %s :What do you want me to do ?\n",&BOTNAME,CmdDo.user->nick);writeln(buf); }
}

void do_raw()
{
  if(CmdDo.param[0])
    { sprintf(buf,"%s\n",CmdDo.param);writeln(buf); }
  else
    { sprintf(buf,":%s PRIVMSG %s :What do you want me to send ?\n",&BOTNAME,CmdDo.user->nick);writeln(buf); }
}

void do_broadcast()
{
  if(CmdDo.param[0])
    Srvbroadcast(CmdDo.param);
  else
    { sprintf(buf,":%s PRIVMSG %s :What do you want me to say ?\n",&BOTNAME,CmdDo.user->nick);writeln(buf); }
}

void do_who()
{
  Srvwho(CmdDo.user->nick);
}

void do_join()
{
  sChannelList *chanstr,chanstrptr;
  chanstr = &chanstrptr;
  
  if(CmdDo.param[0]) 
  {
      if (!channels.findChannel(CmdDo.param,chanstr))
      {
	  send_to_user(CmdDo.user->nick,"Sorry, can not join unregistered hannel %s.",CmdDo.param);
	  return;
      }
  }
  else
  {
      channels.findChannel(CmdDo.channel,chanstr);
  }

  if(chanstr!=0) {
    // sprintf(buf,":%s JOIN %s\n",&BOTNAME,chanstr->name);writeln(buf);
    // sprintf(buf,":%s MODE %s +o %s\n",&SERVNAME,chanstr->name,&BOTNAME);writeln(buf);
    sprintf(buf,":%s SJOIN %ld %s +tn  :@%s\n",&SERVNAME,chanstr->created,chanstr->name, &BOTNAME);
    writeln(buf);
    sprintf(buf,":%s MODE %s +o %s\n",&SERVNAME,chanstr->name,&BOTNAME);
    writeln(buf);
  }
}

void do_part()
{
  if(CmdDo.param[0]) {
    sprintf(buf,":%s PART %s\n",&BOTNAME,CmdDo.param);writeln(buf);
  }
  else {
    sprintf(buf,":%s PART %s\n",&BOTNAME,CmdDo.channel);writeln(buf);
  }
}

void do_cleanchan()
{
  sprintf(buf,"MODE %s +nt-liskm *\n",CmdDo.channel);writeln(buf);
}

void do_topic()
{
  char topic[TOPICLEN];

  if(CmdDo.chanstr->name[0]!=0) {
    if(CmdDo.param[0]) {
      strncpy(topic,CmdDo.param,TOPICLEN);
      sprintf(buf,":%s TOPIC %s :%s\n",&BOTNAME,CmdDo.channel,topic);
      channels.setTopic(CmdDo.channel,topic);
    }
    else {
      sprintf(buf,":%s TOPIC %s :\n",&BOTNAME,CmdDo.channel);
      channels.setTopic(CmdDo.channel,"");
    }
    writeln(buf);
  }
  else {
    send_to_user(CmdDo.user->nick,"No channel by that name!");
  }
}

void do_onjoin()
{
  int line,i;
  char *sline;
  char *text;
  char onjointext[ONJOINLEN];

  if(CmdDo.chanstr->name[0]!=0) {
     if((sline = get_token(&CmdDo.param, ",: "))!=NULL) {
       line = atoi(sline);
       if(line==0)
          for(i=0; i!=5; i++) {
             channels.setOnJoin(CmdDo.channel,i+1,"");
          }
       if((line<=5) && (line>0)) {
         if(CmdDo.param[0])
         {
           strncpy(onjointext,CmdDo.param,ONJOINLEN);
           channels.setOnJoin(CmdDo.channel,line,onjointext);
         }
         else
           channels.setOnJoin(CmdDo.channel,line,"");
       }
     }
     else
       // View current settings
       for(i = 0,line=1; i != 5; i++, line++) {
          if(CmdDo.chanstr->onjoin[i]!=NULL)
             send_to_user(CmdDo.user->nick,"%d: %s",line,CmdDo.chanstr->onjoin[i]);
       }
   }
   else send_to_user(CmdDo.user->nick,"No channel by that name!");
}

void do_chaninfo()
{
  if(CmdDo.chanstr->name[0]!=0) {
    sprintf(buf,":%s NOTICE %s :Channel info for %s\n",&BOTNAME,CmdDo.user->nick,CmdDo.chanstr->name);writeln(buf);
    sprintf(buf,":%s NOTICE %s :     Owner: %s\n",&BOTNAME,CmdDo.user->nick,CmdDo.chanstr->owner);writeln(buf);
    sprintf(buf,":%s NOTICE %s :   Created: %s",&BOTNAME,CmdDo.user->nick,ctime(&CmdDo.chanstr->created));writeln(buf);
//  sprintf(buf,":%s NOTICE %s :     Flags: %s\n",&BOTNAME,CmdDo.user->nick,CmdDo.chanstr->flags);writeln(buf);
    if(CmdDo.chanstr->strictt == TRUE)
    {
      sprintf(buf,":%s NOTICE %s : Keeptopic: True\n",&BOTNAME,CmdDo.user->nick);writeln(buf);
    }
    else
    {
      sprintf(buf,":%s NOTICE %s : Keeptopic: False\n",&BOTNAME,CmdDo.user->nick);writeln(buf);
    }
    sprintf(buf,":%s NOTICE %s :     Topic: %s\n",&BOTNAME,CmdDo.user->nick,CmdDo.chanstr->topic);writeln(buf);
  }
  else
    send_to_user(CmdDo.user->nick,"No channel by that name!");
}  

void do_keeptopic()
{
  if(CmdDo.chanstr->name[0]!=0) {
    if(CmdDo.param[0]) {
      if(CmdDo.param[0]=='T' || CmdDo.param[0]=='t')
      {
        channels.setKeepTopic(CmdDo.chanstr->name, 1);

	sprintf(buf,":%s TOPIC %s :%s\n",&BOTNAME,CmdDo.channel,
		CmdDo.chanstr->topic);
	writeln(buf);
	send_to_user(CmdDo.user->nick,"Topic protection enabled.");
      }
      else {
        channels.setKeepTopic(CmdDo.chanstr->name, 0);
	send_to_user(CmdDo.user->nick,"Topic protection disabled.");
      }
    }
    else {
      send_to_user(CmdDo.user->nick,"Required switch missing (T/F)");
    }
  }
  else {
    send_to_user(CmdDo.user->nick,"No channel by that name!");
  }
}

void show_commands()
{
  int i,clevel,n;
  char cmdline[255];
  clevel=-1;
  n=0;
  send_to_user(CmdDo.user->nick,"\002Level  Command\002");
  for(i = 0; on_msg_commands[i].name != NULL; i++) {
    if(CmdDo.user->level >= on_msg_commands[i].ulevel) {
      if(clevel!=on_msg_commands[i].ulevel) {
         if(clevel!=-1) send_to_user(CmdDo.user->nick,cmdline);
         clevel=on_msg_commands[i].ulevel;
	 sprintf(cmdline,"%5d  : ",clevel);
	 n=0;
      }
      n++;
      if (n==1) sprintf(buf,"%s",on_msg_commands[i].name);
      if (n!=1) sprintf(buf,", %s",on_msg_commands[i].name);
      strcat(cmdline,buf);
      // send_to_user(CmdDo.user->nick,buf);
    }               
  }                 
send_to_user(CmdDo.user->nick,cmdline);
}

void show_help()
{
  int     i, j;
  FILE    *f;
  char    *s;

  //send_to_user(CmdDo.user->nick,"This function has been disabled untill the help bug has been corrected.");
  //return;

  if((f=fopen((char *)&HELPFILE, "r"))==NULL)
    {
      send_to_user(CmdDo.user->nick, "Helpfile missing");
      return;
    }

  if(CmdDo.param[0] == '\0')
    {
      find_topic(f, "standard");
      send_to_user(CmdDo.user->nick,"\026 HELP on standard                                                \026");
      while(s=get_ftext(f))
	send_to_user(CmdDo.user->nick, s);
    }
  else
    if(!find_topic( f, CmdDo.param ))
      send_to_user(CmdDo.user->nick, "No help available for \"%s\"", CmdDo.param);
    else {
      j=-1;
      for(i = 0; on_msg_commands[i].name != NULL; i++)
	if(STRCASEEQUAL(on_msg_commands[i].name,CmdDo.param)) j=on_msg_commands[i].ulevel;
      if(j==-1)
	send_to_user(CmdDo.user->nick,"\026 HELP on %-34s                      \026",CmdDo.param);
      else
	send_to_user(CmdDo.user->nick,"\026 HELP on %-34s Minimum access: %4d \026",CmdDo.param,j);
      while(s=get_ftext(f))
	send_to_user(CmdDo.user->nick, s);
    }
  fclose(f);
}

void do_op()
{
  char *nick;
  if((nick = get_token(&CmdDo.param, ",: "))!=NULL) {
  // if(CmdDo.param[0]) {
    sprintf(buf,":%s MODE %s +o %s\n",(char *)&BOTNAME,CmdDo.channel,nick);
  }
  else
  {
    sprintf(buf,":%s MODE %s +o %s\n",(char *)&BOTNAME,CmdDo.channel,CmdDo.user->nick);
  }
  writeln(buf);
}

void do_voice()
{
  char *nick;

  if((nick = get_token(&CmdDo.param, ",: "))!=NULL) {
  // if(CmdDo.param[0]) {
    sprintf(buf,":%s MODE %s +v %s\n",(char *)&BOTNAME,CmdDo.channel,nick);
  } 
  else
    sprintf(buf,":%s MODE %s +v %s\n",(char *)&BOTNAME,CmdDo.channel,CmdDo.user->nick);
  writeln(buf);
}

void do_inviteme()
{
  sprintf(buf,":%s INVITE %s %s\n",(char *)&BOTNAME,CmdDo.user->nick,CmdDo.channel);
  writeln(buf);
}

void do_deop()
{
  sUserList *tmpuser,tmpuserptr;
  char *nick;
  tmpuser=&tmpuserptr;

  if((nick = get_token(&CmdDo.param, ",: "))!=NULL) {
    if(nicks.findUser(CmdDo.channel,nick,tmpuser))
      if(tmpuser->level>=CmdDo.user->level)
      {
	send_to_user(CmdDo.user->nick,"You can not deop a user with the same/higher level than yourself!");
	return;
      }
    if(STRCASEEQUAL(nick,(char *)&BOTNAME))
    {
	send_to_user(CmdDo.user->nick,"You can not deop me, your idiot!");
	return;
    }       
    sprintf(buf,":%s MODE %s -o %s\n",(char *)&BOTNAME,CmdDo.channel,nick);
  }
  else
    sprintf(buf,":%s MODE %s -o %s\n",(char *)&BOTNAME,CmdDo.channel,CmdDo.user->nick);
  writeln(buf);
}

void do_kick()
{
  sUserList *tmpuser,tmpuserptr;
  char *nick;
  char *comment;
  tmpuser = &tmpuserptr;

  if((nick = get_token(&CmdDo.param, ",: "))!=NULL) {
    if(nicks.findUser(CmdDo.channel,nick,tmpuser))
      if(tmpuser->level>=CmdDo.user->level) {
	send_to_user(CmdDo.user->nick,"You can not kick a user with the same/higher level than yourself!");
	return;
      }
    if(STRCASEEQUAL(nick,(char *)&BOTNAME)) {
	send_to_user(CmdDo.user->nick,"You can not kick me, you motherless son of a goat!");
	return;
    }       
    if(CmdDo.param[0])
       sprintf(buf,":%s KICK %s %s :%s (Requested by %s)\n",(char *)&BOTNAME,CmdDo.channel,nick,CmdDo.param,CmdDo.user->nick);
    else
       sprintf(buf,":%s KICK %s %s :Requested by %s\n",(char *)&BOTNAME,CmdDo.channel,nick,CmdDo.user->nick);

  }
  else {
    send_to_user(CmdDo.user->nick,"I cant really understand who you want to kick ? Try again pal!");
        return;
  }
  writeln(buf);
}

void do_ban()
{
  sUserList *tmpuser,tmpuserptr;
  sSrvUserList *srvuser;
  char *nick;
  char *bantype;
  char *bantime;
  char *comment;
  char params[500];
  char nickuserhost[500];
  char mask[sizeof(CmdDo.user->mask)];
  send_to_user(CmdDo.user->nick,"Sorry, but the ban function is disabled right now, will come back stronger - soon");
  return;
  tmpuser = &tmpuserptr;
  strcpy(params,CmdDo.param);
putlog ("DEBUG params %s\n",params);
  nick = strtok(params," ");
  if (nick == NULL)
  {
      send_to_user(CmdDo.user->nick,"Please use correct BAN syntax: BAN <nick> [<time> [<type> [<Reason text>]]].");
      return;
  }
putlog ("DEBUG nick %s\n",nick);
  bantime = strtok(NULL," ");
putlog ("DEBUG bantime %s\n",bantime);
  bantype = strtok(NULL," ");
putlog ("DEBUG bantype %s\n",bantype);
  comment = params;
putlog ("DEBUG comment %s\n",comment);

  return;

  if((nick = get_token(&CmdDo.param, ",: "))!=NULL) {
    if(nicks.findUser(CmdDo.channel,nick,tmpuser))
      if(tmpuser->level>=CmdDo.user->level) {
        send_to_user(CmdDo.user->nick,"You can not ban a user with the same/higher level than yourself!");
        return;
      }
    if(STRCASEEQUAL(nick,(char *)&BOTNAME)) {
        send_to_user(CmdDo.user->nick,"You can not ban me, you motherless son of a goat!");
        return;
    }

    if((srvuser=Srvfind_user(nick))==NULL) {
        send_to_user(CmdDo.user->nick,"I can not see that user here!");
        return;
    }

    int ii=0;
    sprintf(nickuserhost,"*!*%s",nick,srvuser->host);

    banmask(nickuserhost,mask);

    sprintf(buf,":%s MODE %s +b %s\n",(char *)&BOTNAME,CmdDo.channel,mask);
    writeln(buf);

    if(CmdDo.param[0])
       sprintf(buf,":%s KICK %s %s :%s (Requested by %s)\n",(char *)&BOTNAME,CmdDo.channel,nick,CmdDo.param,CmdDo.user->nick);
    else
       sprintf(buf,":%s KICK %s %s :(Requested by %s)\n",(char *)&BOTNAME,CmdDo.channel,nick,CmdDo.user->nick);
    writeln(buf);
  }  
  else {
    send_to_user(CmdDo.user->nick,"I cant realy understand who you want to ban ? Try again pal!");
        return;
  }
}

void do_devoice()
{
  sUserList *tmpuser,tmpuserptr;
  char *nick;
  tmpuser = &tmpuserptr;

  if((nick = get_token(&CmdDo.param, ",: "))!=NULL) {
    if(nicks.findUser(CmdDo.channel,nick,tmpuser))
      if(tmpuser->level>=CmdDo.user->level) {
        send_to_user(CmdDo.user->nick,"You can not devoice a user with the same/higher level than yourself!");
        return;
      }
    if(STRCASEEQUAL(nick,(char *)&BOTNAME)) {
        send_to_user(CmdDo.user->nick,"You can not devoice me, your idiot!");
        return;
    }  
    sprintf(buf,":%s MODE %s -v %s\n",(char *)&BOTNAME,CmdDo.channel,nick);
  } 
  else
    sprintf(buf,":%s MODE %s -v %s\n",(char *)&BOTNAME,CmdDo.channel,CmdDo.user->nick);
  writeln(buf);
}   

void do_userlist()
{
  sChannelList *tmpchan, tmpchanstr;
  tmpchan = &tmpchanstr;

  if(channels.findChannel(CmdDo.channel,tmpchan))
  {
     if((CmdDo.user->level<450) && (CmdDo.clevel<450))
       nicks.showUsers(CmdDo.channel, CmdDo.user->nick);
     else
       nicks.showUsersDetail(CmdDo.channel, CmdDo.user->nick);
  }
  else
     send_to_user(CmdDo.user->nick,"Channel not found!\n");
}

void do_userinfo()
{
  sUserList *tmpuser, tmpuserstr, *cuser;
  tmpuser = &tmpuserstr;

  if(nicks.findUser(CmdDo.channel,CmdDo.param,tmpuser))
  {
    send_to_user(CmdDo.user->nick," Channel: %s\n",CmdDo.channel);
    send_to_user(CmdDo.user->nick,"    Nick: %s\n",tmpuser->nick);
    if ((CmdDo.user->level>=450) || (CmdDo.clevel>=450))
    {
      send_to_user(CmdDo.user->nick,"  Ulevel: %d\n",tmpuser->level);
      send_to_user(CmdDo.user->nick,"Authchan: %s\n",tmpuser->authchan);
    }
    send_to_user(CmdDo.user->nick,"  AutoOp: %s\n",tmpuser->flags);
    send_to_user(CmdDo.user->nick," Lastlog: %s\n",ctime(&tmpuser->seen));
    send_to_user(CmdDo.user->nick,"    Mask: %s\n",tmpuser->mask);
    send_to_user(CmdDo.user->nick," Comment: %s\n",tmpuser->comment);
  }
  else send_to_user(CmdDo.user->nick,"User not found!\n");
}

void do_savecache()
{
  //  Srvshowusers_();
  //  printf("\nKanaler:\n");
  //  show_channels();
  //save_channels();
  send_to_user(CmdDo.user->nick,"This function is no longer valid.");
}

void do_server()
{
// :Oslo.NO.EU.IRCLink.Net SERVER Services.IRCLink.Net 2 0 911650984 P09 ]]] :[oslo.no.eu.irclink.net] IRCLink Services
// :Oslo.NO.EU.IRCLink.Net SERVER Tonsberg.NO.EU.IRCLink.Net 2 0 911650680 P10 CD] :[193.215.168.141] Tonsberg, Norway
  if(CmdDo.param[0])
    {sprintf(buf,"SERVER %s 1 0 %ld P10 FD] :Juped by %s\n",CmdDo.param,time(NULL),CmdDo.user->nick);writeln(buf);}
  else
    send_to_user(CmdDo.user->nick,"Required parameter missing");
}

void do_say()
{
  if(CmdDo.param[0])
    send_to_channel(CmdDo.channel,CmdDo.param);
  else
    send_to_user(CmdDo.user->nick,"Required parameter missing");
}

void do_act()
{
  if(CmdDo.param[0]) {
    sprintf(buf,":%s PRIVMSG %s :\1ACTION %s\1\n",(char *)&BOTNAME,CmdDo.channel,CmdDo.param);
    writeln(buf);
  }
  else
    send_to_user(CmdDo.user->nick,"Required parameter missing");
}

void do_comment()
{
  if(CmdDo.param[0]) {
    *(CmdDo.param+(sizeof(CmdDo.user->comment)-2))='\0';
    nicks.setComment(CmdDo.channel,CmdDo.user->nick,CmdDo.param);
    send_to_user(CmdDo.user->nick,"Added comment string!");
  }
  else {
    nicks.setComment(CmdDo.channel,CmdDo.user->nick,"");
    send_to_user(CmdDo.user->nick,"Cleared comment string!");
  }
}

void do_authchan()
{
  if(CmdDo.param[0]) {
    *(CmdDo.param+(sizeof(CmdDo.user->authchan)-2))='\0';
    nicks.setAuthChan(CmdDo.channel,CmdDo.user->nick,CmdDo.param);
    send_to_user(CmdDo.user->nick,"Added auth channel");
  }
  else {
    nicks.setAuthChan(CmdDo.channel,CmdDo.user->nick,"");
    send_to_user(CmdDo.user->nick,"Cleared auth channel");
  }
}

void do_autoop()
{
  if(CmdDo.param[0]) {
    if(CmdDo.param[0]=='T' || CmdDo.param[0]=='t') {
      nicks.setAutoOp(CmdDo.channel,CmdDo.user->nick,"1");
      send_to_user(CmdDo.user->nick,"Autoop enabled.");
    }
    else {
      nicks.setAutoOp(CmdDo.channel,CmdDo.user->nick,"0");
      send_to_user(CmdDo.user->nick,"Autoop disabled.");
    }
  }
  else {
    send_to_user(CmdDo.user->nick,"Required switch missing (T/F)");
  }
}

// Addchan: channel, chanmaster
void do_addchan()
{
  sChannelList *tmpchan, tmpchanstr;
  sUserList *tmpuser, tmpuserstr;
  char *channel;
  char *nick;
  tmpchan = &tmpchanstr;
  tmpuser = &tmpuserstr;

  if((channel = get_token(&CmdDo.param, ",: "))!=NULL)
    if((nick = get_token(&CmdDo.param, ",: "))!=NULL) {
      if(!channels.findChannel(channel,tmpchan))
      {	
	if ( validate_channel_name(channel) != True )
	{
	   send_to_user(CmdDo.user->nick,"Illegal Channel Name, must start with #, Contain A-z 0-9 - _ . and max 50 Characters.\n");
	   return;
	}
	if(!Srvfind_user(nick)) 
	{
	   send_to_user(CmdDo.user->nick,"Unable to create, the channel master (%s) is not online!",nick);
	   return;
	}
	send_to_user(CmdDo.user->nick,"Creating channel '%s', with master: %s\n",channel,nick);
       
        channels.clearProperties();
        channels.addChannel(channel,nick); 
        if(!channels.findChannel(channel,tmpchan))
	{
	     send_to_user(CmdDo.user->nick,"An unknown error occured while creating channel, aborted, sorry, I'm not perfect.");
	     putlog("DEBUG: failed to create channel %s for user %s\n",channel,nick);
	     return;
	}

        sprintf(buf,":%s SJOIN %ld %s +tno :@%s\n",&SERVNAME,tmpchan->created,tmpchan->name, &BOTNAME);
	writeln(buf);
	sprintf(buf,"MODE %s +o %s\n",tmpchan->name,(char *)&BOTNAME);writeln(buf);	

	send_to_user(nick,"Channel %s was successfully registered to you. Please use /msg %s COMMANDS %s for a list of available commands.",
				tmpchan->name,(char *)&BOTNAME,tmpchan->name);

	// auto-add user in channel.. hopefully this does not cause trouble.. (Stoke 2004-11-02)
	CmdDo.channel=channel;
	sprintf(buf,"%s %d",nick,500);
	CmdDo.param=buf;
	do_adduser();


      }
      else send_to_user(CmdDo.user->nick,"Channel '%s' is already registred!",tmpchan->name);
    }
}

void do_remchan()
{
  sChannelList *tmpchan, tmpchanstr;
  char *channel;
  tmpchan = &tmpchanstr;

  if((channel = get_token(&CmdDo.param, ",: "))!=NULL)
    if(channels.findChannel(channel,tmpchan))
    {
      if(channels.remChannel(channel)) {
         send_to_user(CmdDo.user->nick,"Channel '%s' removed\n",channel);
         sprintf(buf,":%s PART %s\n",(char *)&BOTNAME,channel);writeln(buf);
      }
      else send_to_user(CmdDo.user->nick,"Error removing channel.\n");      
    }
    else send_to_user(CmdDo.user->nick,"Channel not found!\n");
}

void do_adduser()
{
  char *nick;
  char mask[sizeof(CmdDo.user->mask)];
  char *level;
  sSrvUserList *suser;
  sUserList *tmpuser, tmpuserptr;
  tmpuser=&tmpuserptr;
// comment out when not debuging
	printf("CmdDo.user->level: %d\n",CmdDo.user->level);
	//printf("CSERVICE: %s\n",(char)&CSERVICE);
	printf("CmdDo.channel: %s\n",CmdDo.channel ? CmdDo.channel : "");
	printf("CmdDo.clevel: %d\n",CmdDo.clevel);
	printf("CmdDo.cmdlevel: %d\n",CmdDo.cmdlevel);
	printf("CmdDo.param: %s\n",CmdDo.param ? CmdDo.param : "");
	printf("---------------\n");
/////////////////////////////////////////

  if((nick = get_token(&CmdDo.param, ",: "))!=NULL)
      if((level = get_token(&CmdDo.param, ",: "))!=NULL) {
	if(suser=Srvfind_user(nick)) {
	  if(CmdDo.user->level <= atoi(level))
	    if((CmdDo.clevel>=CmdDo.cmdlevel) && (strcmp(CmdDo.channel,(char *)&CSERVICE)) &&
               (CmdDo.clevel>=atoi(level)))
	      send_to_user(CmdDo.user->nick,"Userlevel too low. Executing with Channel Service level!");
	    else
		 {
	      send_to_user(CmdDo.user->nick,"You can not add a user with the same/higher level than yourself!");
	      return;
	    }
	}
	else {
	  send_to_user(CmdDo.user->nick,"Can not add user, he/she is currently not online!");
	  return;
	}

        if(nicks.findUser(CmdDo.channel,nick,tmpuser))
        {
           send_to_user(CmdDo.user->nick,"User already exists!\n");
           return;
        }


        // If user is cservice but not level 700+ he wont be able to add a cservice user!
	if(STRCASEEQUAL(CmdDo.channel,(char *)&CSERVICE) && (CmdDo.clevel < 700))
	{
	  send_to_user(CmdDo.user->nick,"You are not authorized to add a user to %s. Your attempt have been logged!\n",
                       (char *)&CSERVICE);
          putlog("[Warning] %s tryed to add %s to %s!\n",CmdDo.user->nick,nick,CmdDo.channel);
          return;
        }

        // You can not add a 500+ user if your not a 700+ cservice user.
	if(!STRCASEEQUAL(CmdDo.channel,(char *)&CSERVICE) && (CmdDo.clevel < 700) && (atoi(level) > 500))
        {
          send_to_user(CmdDo.user->nick,"500 is the max level for normal channels. Your attempt have been logged!\n");
          putlog("[Warning] %s tryed to add %s to %s at level %s!\n",CmdDo.user->nick,nick,CmdDo.channel,level);
          return;
        }
printf("jsdebug: adding user\n");
        nicks.clearProperties();
        ipmask(suser->host,nicks.user.mask);
        nicks.user.level = atoi(level);
        nicks.user.authed = suser->logon;
printf("jsdebug: final...adding it via nicks\n");
        nicks.addUser(CmdDo.channel,nick);

	//cmd_add_user(CmdDo.channel,nick,mask,atoi(level),suser->logon);
	send_to_user(CmdDo.user->nick,"Added user '%s' with access %s to channel %s.",nick,level,CmdDo.channel);
	send_to_user(nick,"You have been added to channel %s, with level %s.",CmdDo.channel,level);
	send_to_user(nick,"Please type '/msg %s setpass %s <password>' now!",(char *)&BOTNAME,CmdDo.channel);
	send_to_user(nick,"If you do not do so, you will not have access next time you log on!");
	send_to_user(nick,"For more info, type /msg %s help setpass",(char *)&BOTNAME);
      }
}

void do_remuser()
{
  sUserList *tmpuser, tmpuserptr;
  tmpuser = &tmpuserptr;

  if(nicks.findUser(CmdDo.channel,CmdDo.param,tmpuser))
  {
    if(CmdDo.user->level <= tmpuser->level) {
      if(CmdDo.clevel>=CmdDo.cmdlevel) send_to_user(CmdDo.user->nick,"Userlevel too low. Executing with Channel Service level!");
      else {
	send_to_user(CmdDo.user->nick,"You can not remove a user with the same userlevel or higher than yours!\n");
	return;
      }
    }
    if(nicks.remUser(CmdDo.channel,CmdDo.param)) send_to_user(CmdDo.user->nick,"User '%s' removed\n",tmpuser->nick);
    else send_to_user(CmdDo.user->nick,"Error removing user.\n");
  }
  else send_to_user(CmdDo.user->nick,"User not found!\n");
}

void do_deauth()
{
    nicks.deauthUser(CmdDo.channel,CmdDo.user->nick); 
    send_to_user(CmdDo.user->nick,"Your host is now deauthorized!");
}

void do_auth()
{
  sSrvUserList *suser;
  sUserList *user,tmpuser;
  user=&tmpuser;

  if(CmdDo.param[0]) {
    if(nicks.findUser(CmdDo.channel,CmdDo.user->nick,user))
    {
      if(STRCASEEQUAL(CmdDo.param,user->passwd))
      {
	if(suser=Srvfind_user(CmdDo.user->nick))
        {
          nicks.authUser(CmdDo.channel,CmdDo.user->nick,suser);
	  send_to_user(CmdDo.user->nick,"Your host is now authorized!");
	}
	else
	  send_to_user(CmdDo.user->nick,"Access denied! (Can't find you on the net!)");
      }
      else
	send_to_user(CmdDo.user->nick,"Access denied!");
    }
    else
      send_to_user(CmdDo.user->nick,"Sorry, but I can't find you in that channel, sir!");
  }
  else
    send_to_user(CmdDo.user->nick,"Usage: /msg %s auth <channel> <password>",(char *)&BOTNAME);
}

void do_setpass()
{
  char passwd[PASSWDLEN];

  if(CmdDo.param[0]) {
    strncpy(passwd,CmdDo.param,PASSWDLEN);
    nicks.setPasswd(CmdDo.channel,CmdDo.user->nick,passwd);
    //strncpy(CmdDo.user->passwd,CmdDo.param,sizeof(CmdDo.user->passwd));
    send_to_user(CmdDo.user->nick,"Password changed");
  }
  else
    send_to_user(CmdDo.user->nick,"Usage: /msg %s setpass <channel> <password>",(char *)&BOTNAME);
}

void do_setlevel()
{
  char *nick, *level;
  sUserList *user,userptr;
  user = &userptr;

  if((nick = get_token(&CmdDo.param, ",: "))==NULL) {
    send_to_user(CmdDo.user->nick,"Usage: setlevel <nick> <level>");
    return;
  }

  if((level = get_token(&CmdDo.param, ",: "))==NULL) {
    send_to_user(CmdDo.user->nick,"Usage: setlevel <nick> <level>");
    return;
  }

  if(!nicks.findUser(CmdDo.channel,nick,user)) {
    send_to_user(CmdDo.user->nick,"No user by that name!");
    return;
  }

  if(atoi(level) == 0) {
    send_to_user(CmdDo.user->nick,"You have specified a illegal level!");
    return;
  }

  if((CmdDo.user->level <= atoi(level)) && !((CmdDo.clevel >= CmdDo.cmdlevel) && (atoi(level) < CmdDo.clevel))) {
    send_to_user(CmdDo.user->nick,"You can't set a user's level higher or same as your own!");
    return;
  }

  if((CmdDo.user->level <= user->level) && !((CmdDo.clevel >= CmdDo.cmdlevel) && (user->level < CmdDo.clevel))) {
    send_to_user(CmdDo.user->nick,"You cant change a user with same or higher level than you!");
    return;
  }

  // You can not change level to more than 500 if your not a 700+ cservice user.
  if((CmdDo.clevel < 700) && (atoi(level) > 500))
  {
    send_to_user(CmdDo.user->nick,"500 is the max level for normal channels. Your attempt have been logged!\n"); 
    putlog("[Warning] %s tryed to change level on %s in %s to %s!\n",CmdDo.user->nick,nick,CmdDo.channel,level);
    return;
  }

  nicks.setLevel(CmdDo.channel,nick,atoi(level));
  send_to_user(CmdDo.user->nick,"New level: %d, added to user %s",atoi(level),nick);
}

void do_resync()
{
   send_to_user(CmdDo.user->nick,"Syncing %s with the other servers\n",(char *)&BOTNAME);
   channels.resyncChannels();
   send_to_user(CmdDo.user->nick,"Sync is complete!\n");
}

void do_mode()
{
  if(CmdDo.param[0])
  {
     sprintf(buf,"MODE %s\n",CmdDo.param);
     writeln(buf);
     send_wallops("%s is using %s to mode %s", CmdDo.user->nick, &SERVNAME, CmdDo.param);
  }
  else
  {
     sprintf(buf,":%s PRIVMSG %s :What modes do you want me to change ?\n",&BOTNAME,CmdDo.user->nick);
     writeln(buf);
  }
}

