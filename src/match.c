/************************************
 *  match(string1,string2)          *
 *  wild card matching routine      *
 *  origionally for Ultima1.0       *
 *  Written by WildThang            *
 *  dvmitche@midway.ecn.uoknor.edu  *
 *  based on ideas by hendrix       *
 ************************************/

/* int do_match(char *string1,char *string2)
   returns 0 if match
   returns 1 if do not match */


#include <stdio.h>
#include <ctype.h>

int match(char *check,char *orig)
{
	if(*orig == '~') ++orig;
	while(*check == '*' ||
		tolower(*check)==tolower(*orig) ||
		*check == '?')
 	if(*check == '*')
	   if(*++check) {
	   	while(*orig)
	      		if(!match(check,orig++)) return 0;
	   	return 1;
	   	}
  	   else
	   	return 0;
	   else if (!*check)
	   	return 0;
	   else if (!*orig)
	   	return 1;
	   else {
			++check;
			++orig;
		}
	return 1;
}

