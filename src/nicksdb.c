//
// W SQL Connection routines
// (C) 1998 by Jon Suphammer
// Converted to use mysql 4.x compliant functions by J.Thomas Stokkeland 2004 (Stoke)
// 
// $Id: nicksdb.c 4 2004-11-09 06:48:12Z stoke $
// 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
//#include "db.h"
#include "config.h"
#include "global.h"
#include <time.h>

boolean nicksclass::open()
{
   results=NULL;

   if (mysql_init(&mysql) == NULL) {
       db_error("Unable to initialize MySQL client structure (nick)\n");
       return False;
   }
   if(mysql_real_connect(&mysql,(char *)&DBHOST,nit_user,nit_pass,(char *)&DBNAME,0,NULL,0) == NULL ) {
       db_error("Unable to connect to SQL server (nick)\n");
       return False;
   }
   return True;

}

boolean nicksclass::close()
{
   if(results!=0) {
      mysql_free_result(results);results=NULL; 
   }
   mysql_close(&mysql);
}

boolean nicksclass::findUser(char *channel, char *nick, sUserList *dest)
{
   char myquery[CHANNELLEN+NICKLEN+4+255];
   char mychan[MAXLEN],mynick[MAXLEN];

   if(results!=0) {
      mysql_free_result(results);results=NULL;
   }

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));

   sprintf(myquery,"select channel,nick,level,flags,seen,passwd,authed,comment,mask,authchan from nicks where channel like '%s' AND nick like '%s'",
      mychan,
      mynick);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Error doing query\n");
      return False;
   }

   results=mysql_store_result(&mysql);
   rowcnt=mysql_num_rows(results);
   currow=mysql_fetch_row(results);
   if(rowcnt==0)
      return False;

   strncpy(dest->nick,currow[nt_nick],NICKLEN);
   strncpy(dest->channel,currow[nt_channel],CHANNELLEN);
   dest->level = (int)atoi(currow[nt_level]);
   strncpy(dest->flags,currow[nt_flags],UFLAGSLEN);
   dest->seen = atol(currow[nt_seen]);
   strncpy(dest->passwd,currow[nt_passwd],PASSWDLEN);
   dest->authed = atol(currow[nt_authed]);
   strncpy(dest->comment,currow[nt_comment],COMMENTLEN);
   strncpy(dest->mask,currow[nt_mask],MASKLEN);
   strncpy(dest->authchan,currow[nt_authchan],CHANNELLEN);
}

boolean nicksclass::addUser(char *channel, char *nick)
{
   char myquery[sizeof(user)+CHANNELLEN+NICKLEN+14+512];
   char mychan[MAXLEN],mynick[MAXLEN],flags[MAXLEN],passwd[MAXLEN],comment[MAXLEN],mask[MAXLEN],authchan[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));
   mysql_real_escape_string(&mysql,flags,user.flags,strlen(user.flags));
   mysql_real_escape_string(&mysql,passwd,user.passwd,strlen(user.passwd));
   mysql_real_escape_string(&mysql,comment,user.comment,strlen(user.comment));
   mysql_real_escape_string(&mysql,mask,user.mask,strlen(user.mask));
   mysql_real_escape_string(&mysql,authchan,user.authchan,strlen(user.authchan));

   printf("jsdebug: adding query for adduser\n");
   sprintf(myquery,"INSERT INTO nicks VALUES ('%s','%s',%d,'%s',%ld,'%s',%ld,'%s','%s','%s')",
	   channel,
	   nick,
           user.level,
	   flags,
           (long *)time(NULL),
	   passwd,
           user.authed,
	   comment,
	   mask,
	   authchan
	  );
printf("jsdebug: doing query for adduser\n");
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed adding user. Query:\n  %s\n",myquery);
      return False;
   }
}

boolean nicksclass::addUserExport(char *channel, char *nick)
{
   char myquery[sizeof(user)+CHANNELLEN+NICKLEN+14+512];
   char mychan[MAXLEN],mynick[MAXLEN],flags[MAXLEN],passwd[MAXLEN],comment[MAXLEN],mask[MAXLEN],authchan[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));
   mysql_real_escape_string(&mysql,flags,user.flags,strlen(user.flags));
   mysql_real_escape_string(&mysql,passwd,user.passwd,strlen(user.passwd));
   mysql_real_escape_string(&mysql,comment,user.comment,strlen(user.comment));
   mysql_real_escape_string(&mysql,mask,user.mask,strlen(user.mask));
   mysql_real_escape_string(&mysql,authchan,user.authchan,strlen(user.authchan));

   sprintf(myquery,"INSERT INTO nicks VALUES ('%s','%s',%d,'%s',%ld,'%s',%ld,'%s','%s','%s')",
	   channel,
	   nick,
           user.level,
	   flags,
           (long *)time(NULL),
	   passwd,
           user.authed,
	   comment,
	   mask,
	   authchan
	  );
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed adding user. Query:\n  %s\n",myquery);
      return False;
   }
}

boolean nicksclass::authUser(char *channel, char *nick, sSrvUserList *suser)
{
   char myquery[CHANNELLEN+NICKLEN+HOSTLEN+8+512];
   char mask[HOSTLEN+256];
   char mychan[MAXLEN],mynick[MAXLEN],mymask[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));

   ipmask(suser->host,mask);
   mysql_real_escape_string(&mysql,mymask,mask,strlen(mask));

   sprintf(myquery,"UPDATE nicks SET authed=%ld, mask='%s', seen=%ld WHERE channel like '%s' AND nick like '%s'",
           suser->logon,
           mymask,
           (long *)time(NULL),
           mychan,
           mynick
	  );

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed authing user.\n");
      return False;
   }
}

boolean nicksclass::deauthUser(char *channel, char *nick)
{
   char myquery[CHANNELLEN+NICKLEN+HOSTLEN+8+512];
   char mychan[MAXLEN],mynick[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));

   sprintf(myquery,"UPDATE nicks SET authed=0, seen=%ld WHERE channel like '%s' AND nick like '%s'",
           (long *)time(NULL), mychan, mynick);
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed deauthing user.\n");
      return False;
   }
}

boolean nicksclass::setPasswd(char *channel, char *nick, char *passwd)
{
   char myquery[CHANNELLEN+NICKLEN+PASSWDLEN+512];
   char mychan[MAXLEN],mynick[MAXLEN],mypass[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));
   mysql_real_escape_string(&mysql,mypass,passwd,strlen(passwd));

   sprintf(myquery,"UPDATE nicks SET passwd='%s' WHERE channel like '%s' AND nick like '%s'",
           mypass, mychan, mynick);
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting user password.\n");
      return False;
   }
}  

boolean nicksclass::setLevel(char *channel, char *nick, int level)
{
   char myquery[CHANNELLEN+NICKLEN+4+512];
   char mychan[MAXLEN],mynick[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));

   sprintf(myquery,"UPDATE nicks SET level=%d WHERE channel like '%s' AND nick like '%s'",
           level,
           mychan,
           mynick);
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting user password.\n");
      return False;
   }
}

boolean nicksclass::setAutoOp(char *channel, char *nick, char *flag)
{
   char myquery[CHANNELLEN+NICKLEN+4+512];
   char mychan[MAXLEN],mynick[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));

   sprintf(myquery,"UPDATE nicks SET flags='%s' WHERE channel like '%s' AND nick like '%s'",
           flag,
           mychan,
           mynick);
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting user autoop.\n");
      return False;
   }
}

boolean nicksclass::setSeen(char *channel, char *nick, long seen)
{
   char myquery[CHANNELLEN+NICKLEN+4+512];
   char mychan[MAXLEN],mynick[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));

   sprintf(myquery,"UPDATE nicks SET seen='%ld' WHERE channel like '%s' AND nick like '%s'",
           seen,
           mychan,
           mynick);
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting user autoop.\n");
      return False;
   }
}

boolean nicksclass::setComment(char *channel, char *nick, char *comment)
{
   char myquery[CHANNELLEN+NICKLEN+COMMENTLEN+4+512];
   char mychan[MAXLEN],mynick[MAXLEN],mycomment[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));
   mysql_real_escape_string(&mysql,mycomment,comment,strlen(comment));

   sprintf(myquery,"UPDATE nicks SET comment='%s' WHERE channel like '%s' AND nick like '%s'",
           mycomment,
           mychan,
           mynick);
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting user comment.\n");
      return False;
   }
}

boolean nicksclass::setAuthChan(char *channel, char *nick, char *authchan)
{
   char myquery[CHANNELLEN+NICKLEN+CHANNELLEN+4+512];
   char mychan[MAXLEN],mynick[MAXLEN],myauthchan[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));
   mysql_real_escape_string(&mysql,myauthchan,authchan,strlen(authchan));

   sprintf(myquery,"UPDATE nicks SET authchan='%s' WHERE channel like '%s' AND nick like '%s'",
           myauthchan,
           mychan,
           mynick);
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting user comment.\n");
      return False;
   }
}

boolean nicksclass::remUser(char *channel, char *nick)
{
   char myquery[CHANNELLEN+NICKLEN+4+512];
   char mychan[MAXLEN],mynick[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mynick,nick,strlen(nick));

   sprintf(myquery,"DELETE FROM nicks WHERE channel like '%s' AND nick like '%s'",
           mychan,
           mynick);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed deleting user\n");
      return False;
   }

   return True;
}

boolean nicksclass::showUsers(char *channel, char *nick)
{
   char myquery[CHANNELLEN+2+512];
   char templine[255];
   long mytime;
   int i;
   char mychan[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));

   if(results!=0) {
      mysql_free_result(results);results=NULL;
   }

   sprintf(myquery,"select * from nicks WHERE channel like '%s' order by seen DESC", mychan);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("showUsers: Error doing query.\n");
      return False;
   }

   results=mysql_store_result(&mysql);
   rowcnt=mysql_num_rows(results);

   sprintf(templine,"Userlist for channel %s:",channel); 
   send_to_user(nick,templine);
   sprintf(templine,"%cName       AOP  Last seen               %c",22,22); 
   send_to_user(nick,templine);

   if(rowcnt==0)
     send_to_user(nick,"No users!"); 

   for(i=0;i<rowcnt;i++)
   {
      currow=mysql_fetch_row(results);
      mytime=atol(currow[nt_seen]);
      sprintf(templine,"%-10s %3s  %s\n",
              currow[nt_nick],
              currow[nt_flags],
              ctime(&mytime));
      send_to_user(nick,templine);

   }
}

boolean nicksclass::showUsersDetail(char *channel, char *nick)
{
   char myquery[CHANNELLEN+2+512];
   char templine[255];
   long mytime;
   int i;
   char mychan[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));

   if(results!=0) {
      mysql_free_result(results);results=NULL;
   }

   sprintf(myquery,"select * from nicks WHERE channel like '%s' order by seen DESC", mychan);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("showUsers: Error doing query.\n");
      return False;
   }

   results=mysql_store_result(&mysql);
   rowcnt=mysql_num_rows(results);

   sprintf(templine,"Userlist for channel %s:",channel); send_to_user(nick,templine);
   sprintf(templine,"%cName    Ulevel AOP  Last seen               %c",22,22); send_to_user(nick,templine);

   if(rowcnt==0)
     send_to_user(nick,"No users!");

   for(i=0;i<rowcnt;i++)
   {
      currow=mysql_fetch_row(results);
      mytime=atol(currow[nt_seen]);

      sprintf(templine,"%-10s %3d %3s  %s\n",
              currow[nt_nick],
              atoi(currow[nt_level]),
              currow[nt_flags],
              ctime(&mytime));
      send_to_user(nick,templine);
   }
}

void nicksclass::clearProperties()
{
   user.nick[0] = 0;
   user.channel[0] = 0;
   user.level = 0;
   strcpy(user.flags,"0");
   user.seen = 0;
   user.passwd[0] = 0;
   user.authed = 0;
   user.comment[0] = 0;
   user.mask[0] = 0;
   user.authchan[0] = 0;
}

void db_error(char *format, ...)
{
  va_list msg;

  va_start(msg, format);
  printf(format, msg);
  // putdebuglog(format, msg);
  va_end(msg);
}

