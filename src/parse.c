/*
 * parse.c - server input parsing
 *
 */ 

#include "global.h"
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <time.h>
extern char buf[512];
extern char sockbuf[512];
extern channelsclass channels;
extern nicksclass nicks;

struct
{
	char	*name;
	void	(*function)(char *from, char *rest);
} parse_funcs[] =
{
	{ "PRIVMSG",	parse_privmsg	},
	{ "NICK",	parse_nick	},
	{ "QUIT",	parse_quit	},
	{ "KILL",	parse_kill	},
	{ "SQUIT",	parse_squit	},
	{ "SJOIN",	parse_join	},
	{ "PART",	parse_part	},
	{ "INFO",	parse_info	},
	{ "MODE",	parse_mode	},
	{ "TOPIC",	parse_topic	},
	{ "ERROR",	parse_error	},
        { NULL,         null(void (*)(char *,char *))}
};

void parse_privmsg(char *from, char *rest)
/*
 * This function parses the "rest" when a PRIVMSG was sent by the
 * server. It filters out some stuff and calls the apropriate routine(s)
 * (on_ctcp / on_msg)
 */

{
    	char  	*text;
    	char  	*to;
    	int   	i;
	
    	if( ( text = index( rest, ' ' ) ) != 0 ) 
        	*( text ) = '\0'; text += 2;
  
    	to = rest;
    	if( *text == ':' )
        	*( text++ ) = '\0';

/* This sometimes fails if the string is longer than MAXLEN bytes (the last 
   \001 is gone. Not very interesting, because normal CTCP requests aren't 
   that long....    */
    	if( *text == '\001' && *( text + strlen( text) - 1 ) == '\001' )
    	{
        	*( text++ ) = '\0';
        	*( text + strlen( text ) - 1 ) = '\0';
        	/* on_ctcp( from, to, text );  */
    	}
    	else
		on_msg( from, to, text );
}

void parse_nick(char *from, char *rest)
{
	char	r[MAXLEN], userhost[500];
	char	*nick, *host, *user, *server;

	strcpy(r, rest);
	nick=strtok(r, " ");
	strtok(NULL, " "); strtok(NULL, " "); strtok(NULL, " ");
	user=strtok(NULL, " ");
	host=strtok(NULL, " ");
	sprintf(userhost,"%s@%s",user,host);
	if(from==NULL)
		server=strtok(NULL, " ");
	else
		server=from;

	if(userhost) {
		Srvadduser(nick,userhost,server);
	}
	else
	if(nick) {
		Srvchuser(from,nick);
	}
}

void parse_quit(char *from, char *rest)
{
  // printf("Quit: %s\n",from);
  Srvremuser(from);
  // printf("Quit: %s\n",from);

}

void parse_error(char *from, char *rest)
{
	putlog("[Error] %s\n",rest);
	cleanup("Received error message from HUB.");
}

void parse_kill(char *from, char *rest)
{
	char *nick;
	if((nick = get_token(&rest, ",: "))!=NULL)
	   if(!strcmp(nick,(char *)&BOTNAME))
	      cleanup("Killed.");
}

void parse_squit(char *from, char *rest)
{
	char *name;
        if((name = get_token(&rest, ",: "))!=NULL)
           if(!strcmp(name,(char *)&SERVER)) {
	      sprintf(buf,"SQUIT by %s",from);
              cleanup(buf);
	}
}

void parse_join(char *from, char *rest)
{
  sUserList *user,userptr,*userAuth,userAuthPtr;
  sSrvUserList *srvuser;
  sChannelList *chanstr,chanstrptr;
  int i,line;
  user = &userptr;
  userAuth = &userAuthPtr;
  chanstr = &chanstrptr;
  chanstr->name[0]='\0';
  char *channelptr, *u;
  char channel[CHANNELLEN];

    u = get_token(&rest, " ");
    channelptr = get_token(&rest, " ");
    u = get_token(&rest, " :");
    from = get_token(&rest, " ");
printf("channelptr: %s, %s\n",channelptr, from);

    strncpy(channel,channelptr,CHANNELLEN);
    if(channels.findChannel(channel,chanstr))
    {
       for(i = 0,line=1; i != 5; i++, line++)
       {
          if(chanstr->onjoin[i][0]!=0)
             send_to_user(from,chanstr->onjoin[i]);
       } 

       if(srvuser=Srvfind_user(from))
       {
         if(nicks.findUser(channel,from,user))
         {
            // Setter last seen dato
            //user->seen=(long *)time(NULL);

            // Er dette en authchan bruker ?
            if(user->authchan!="")
            {
              if(nicks.findUser(user->authchan,from,userAuth))
              {
                if(!match(srvuser->host,userAuth->mask))
                {
                   return;
                }
              }
            }
            else
            // Hvis ikke. Er masken riktig ?
            if(!match(srvuser->host,user->mask))
            {
               senddebug("Maskene stemmer ikke overens");
               return;
            }

            if(isauthed(user, srvuser, channel))
            {
               senddebug("User authed");
               nicks.setSeen((char *)channel, (char *)user->nick, (long)time(NULL));
               if(user->flags[0]=='1')
               {
                  senddebug("  Giving user chan-op status.");
                  sprintf(buf,":%s MODE %s +o %s\n",&BOTNAME,channel,user->nick);writeln(buf);
               }
               else
                  senddebug("  AutoOp=FALSE");
               if(strcmp(user->comment,""))
               {
                  send_to_channel(channel,user->comment);
               }
            } else senddebug("Not authed");


         // End of  findUser
         }
       // End of findChannel
       }
    }
}

void parse_part(char *from, char *rest)
{
  sUserList *user;
  sSrvUserList *srvuser;

  if(srvuser=Srvfind_user(from)) {
    //printf("DEBUG: Part from channel %s, by %s.\n",rest,srvuser->nick);
    if(user=find_user_from_host(rest,srvuser->host)) {
       user->seen=time(NULL);
    }
  }

}


// This function is disabled untill it's fixed ;D  --Exion
void parse_info(char *from, char *rest)
{
  sUserList tmpuser;
  strcpy(tmpuser.nick,from);
  
  //show_help(tmpuser,NULL,"info");  
}

void parse(char *line)
{
  char *from; 
  char *command;
  char *rest;
  int  i;

  KILLNEWLINE(line);
  KILLRETURN(line);

  if( *line == ':' ) 
    {
      if( ( command = index( line, ' ' ) ) == 0 )
	return;
      *( command++ ) = '\0';
      from = line + 1; 
    } 
  else 
    {
      command = line;
      from = NULL;
    }
  
  if( ( rest = index( command, ' ' ) ) == 0 )
    return;

  *( rest++ ) = '\0';
  
  if( *rest == ':' )
    *( rest++ ) = '\0';

  for(i=0; parse_funcs[i].name; i++)
    if(STRCASEEQUAL(parse_funcs[i].name, command))
      {
	parse_funcs[i].function(from, rest);
	return;
      } 
}

void parse_mode(char *from, char *rest)
{
  // send_to_user(from,"Debug: %s",rest);
}

void parse_topic(char *from, char *rest)
{
  sChannelList *chanstr,chanstrptr;
  chanstr = &chanstrptr;
  char *channel;
  if((channel = get_token(&rest, ",: "))!=NULL)
    if(channels.findChannel(channel,chanstr))
    {
      //printf("topic change in channel: %s\n",channel);
      if(chanstr->strictt==1)
      {
	sprintf(buf,":%s TOPIC %s :%s\n",&BOTNAME,channel,chanstr->topic);
	writeln(buf);
      }
    }
}

