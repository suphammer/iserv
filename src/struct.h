/* Size definitions */

// Nicks
#define HOSTLEN		63+128
#define NICKLEN		30+128   /* Default 9 */
#define PASSWDLEN	16
#define COMMENTLEN	64
#define MASKLEN         64+128
#define UFLAGSLEN	32

// Channels
#define TOPICLEN        160
#define CHANNELLEN      50
#define ONJOINLEN	128
#define BANMASKLEN	255
#define BANREASONLEN	50

// Other
#define MAXSTRLEN	256

struct TagSrvUserList {
  char *nick;		/* IRC-nickname */
  char *host;		/* username@host */
  char *server;	/* server the user is connected to */
  //long *logon;
  long logon;
  struct TagSrvUserList *next;	/* Next user */
};

struct TagUserList {
  struct TagUserList *next;
  char nick[NICKLEN];
  char channel[CHANNELLEN];
  int level;
  char flags[UFLAGSLEN];
  long seen;
  char passwd[PASSWDLEN];
  long authed;
  char comment[COMMENTLEN];
  char mask[MASKLEN];
  char authchan[CHANNELLEN];
};

struct TagChannelList {
  struct TagChannelList *next;
  char name[CHANNELLEN];
  char owner[NICKLEN];
  char flags[UFLAGSLEN];
  long created;
  int strictt;
  char topic[TOPICLEN];
  char onjoin[5][ONJOINLEN];
  struct TagUserList *ulist;
  struct SrvChannelBan *bantop;
};

// Linked from the channel list
/*
 * The ban status should be one of the following
 *
 */
struct SrvChannelBan {
  struct SrvChannelBan *next;
  char banmask[BANMASKLEN];
  int expiration;
  char status; 
  char setby[MAXSTRLEN];
  char reason[BANREASONLEN];
};

typedef TagSrvUserList sSrvUserList;
typedef TagUserList sUserList;
typedef TagChannelList sChannelList;

