#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

#include "global.h"
#include "config.h"
#include <stdlib.h>

extern u_short IRCPORT;
extern char sockbuf[512],buf[512];
extern char localhost[64];
extern char REALNAME[512];
extern long SRVUPTIME;
int sckfd;

int call_socket(char *hostname)
{
  struct sockaddr_in sa;
  struct hostent *hp;
  int a, s;
  if((hp=gethostbyname(hostname))==NULL) {
    errno=ECONNREFUSED;
    return(-1);
  }
  bzero(&sa, sizeof(sa));
  bcopy(hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
  sa.sin_family = hp->h_addrtype;
  sa.sin_port = htons((u_short)IRCPORT);
  if((s=socket(hp->h_addrtype, SOCK_STREAM, 0)) < 0) return(-1);
  if(connect(s,(struct sockaddr *) &sa, sizeof(sa)) < 0) {
    close(s);
    return(-1);
  }
  return(s);
}

int readln()
{
  int i=0,valid=1;
  char c;
  while(valid) {
    if(read(sckfd,&c,1)<1) return(0);
    if(i<511 && c != '\n') sockbuf[i++]=c;
    else valid=0;
  }
  sockbuf[i]='\0';
  return(1);
}

int senddebug(char *outbuf)
{
#ifdef DEBUG
  char buf[512];
  printf("DEBUG: %s\n",outbuf ? outbuf : "");
  sprintf(buf,":%s PRIVMSG #Channels :DEBUG: %s\n"
  	  ,&BOTNAME, outbuf ? outbuf : "");
//  writeln(buf);
#endif
}

int writeln(char *outbuf)
{
  int to=0;
  if(write(sckfd, outbuf, strlen(outbuf)) < to ) return(0);
  return(1);
}

int send_to_user(char *to, char *format, ...)
{
  char    buf[MAXLEN];
  int     bytessend;
  va_list msg;

  va_start(msg, format);
  vsprintf(buf, format, msg);
  va_end(msg);
  if(to)
    return(sendnotice(to, "%s", buf));
  else
    return(True);
}

int sendnotice(char *sendto, char *format, ...)
{
  char    buf[MAXLEN];
  va_list msg;
  int to=0;
  char templine[MAXLEN];

  va_start(msg, format);
  vsprintf(buf, format, msg);
  va_end(msg);

  sprintf(templine,":%s NOTICE %s :%s\n",&BOTNAME,sendto,buf);
  if(write(sckfd, templine, strlen(templine)) < to ) return(0);
  return(1);
}

int send_to_channel(char *from, char *outbuf)
{
  int to=0;
  char templine[512];
  sprintf(templine,":%s PRIVMSG %s :%s\n",&BOTNAME,from,outbuf);
  if(write(sckfd, templine, strlen(templine)) < to ) return(0);
  return(1);
}

int send_wallops(char *format, ...)
{   
  char    buf[MAXLEN];
  va_list msg;
  int to=0;
  char templine[MAXLEN];

  va_start(msg, format);
  vsprintf(buf, format, msg);
  va_end(msg);

  sprintf(templine,":%s WALLOPS :%s\n",&BOTNAME,buf);
  if(write(sckfd, templine, strlen(templine)) < to ) return(0);
  return(1);
}

int send_to_server(char *format, ...)
{
  char    buf[MAXLEN];
  va_list msg;
  int to=0;
  char templine[MAXLEN];
 
  va_start(msg, format);
  vsprintf(buf, format, msg);
  va_end(msg);

  if(write(sckfd, buf, strlen(buf)) < to ) return(0);
  return(1);
}

int send_join(sChannelList *chan)
{
  int to=0;
  char templine[512];
/*
  printf("Channel: %s\n",chan->name);
  printf("User: %s\n",&BOTNAME);
  printf("Timestamp: %ld\n",chan->created);
*/
  // Join channel
  sprintf(templine,":%s JOIN %s\n",&BOTNAME,chan->name);send_to_server(templine);

  // Make Channel Service operator
  sprintf(templine,"MODE %s +nto %s %ld\n",chan->name,&BOTNAME,chan->created);send_to_server(templine);

  // Check if it's a strict topic channel
  if(chan->strictt==1) {
     sprintf(templine,":%s TOPIC %s :%s\n",&BOTNAME,chan->name,chan->topic);send_to_server(templine);
  }

//sprintf(templine,"MODE %s +o %s\n",chan->name,&BOTNAME);send_to_server(templine);
}

int send_op(sChannelList *chan)
{
  char templine[512];

  // Make Channel Service operator
  sprintf(templine,"MODE %s +nto %s %ld\n",chan->name,&BOTNAME,chan->created);send_to_server(templine);

  // Check if it's a strict topic channel
  if(chan->strictt==1) {
     sprintf(templine,":%s TOPIC %s :%s\n",&BOTNAME,chan->name,chan->topic);send_to_server(templine);
  }

  //sprintf(templine,"MODE %s +o %s\n",chan->name,&BOTNAME);send_to_server(templine);
}

int writeln_srv(char *nick,char *outbuf)
{
  int to=0;
  char templine[512];
  strcpy(templine,":");
  strcat(templine,nick);
  strcat(templine," ");
  strcat(templine,outbuf);
  if(write(sckfd, templine, strlen(templine)) < to ) return(0);
  return(1);
}

boolean login_srv(char *hostname)
{
  putlog("[Info] Trying port %d of %s\n",IRCPORT,hostname);
  if ((sckfd=call_socket(hostname))==-1) {
    putlog("[Info] connection refused, aborting\n", hostname);
    exit(0);
  }

  send_to_server("PASS %s :TS\n",&SERVPASS);
  send_to_server("SERVER %s 1 : %s\n",&SERVNAME,&SERVDESC);
  send_to_server("NICK %s 1 %ld +ki %s %s %s :%s\n", &IRCNAME,time(NULL),&IRCNAME,
          	&SERVNAME,&SERVNAME,&REALNAME);
  putlog("[Info] Connected...\n");
  return True;
}

