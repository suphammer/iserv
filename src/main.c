/************************************************
 * Program:  W
 *
 * Function: Channel Services for IRClink
 *
 * Author:   Jon Suphammer
 * History:  31/12/96 Rev.1 - First release
 *
 *
 ************************************************/

 //  Some edits by J.Thomas Stokkeland (Stoke) 2004/2005
 //
 //  $Id: main.c 4 2004-11-09 06:48:12Z stoke $


#include "global.h"
#include "main.h"
#include <signal.h>
#include <stdlib.h>

int ready;
extern long LASTBACKUP;
long SRVUPTIME;

channelsclass channels;
nicksclass nicks;

void spitout()
{
  int i,num;
  char *temp;
  //sleep(5);
  printf("JSDEBUG: %s\n",sockbuf);
  if(rawdebug) putdebuglog("%s\n",sockbuf);
  if(strncmp(sockbuf,"PI",2)==0) {
    strncpy(sockbuf,"PO",2);
    writeln(strcat(sockbuf,"\n"));
  } /* ping - pong */
  parse(sockbuf);
}

void finishline(int start)
{
  int i;
  i=start;
  while (token[i]) printf(" %s",token[i++]);
}

void openlog()
{
	time_t tloc;

   time(&tloc);
   putlog("\nStarted at %s",ctime(&tloc));
}

void putlog(char *format, ...)
{
  char    buf[MAXLEN];
  time_t tloc;
  va_list msg;
  FILE *logfp;
  tm *my_tm;

  time(&tloc);
  my_tm = localtime(&tloc);
  strftime(buf, sizeof(buf), "/home/iserv/logs/events.%d%m%y", my_tm);

  if((logfp = fopen(buf,"a+")) != NULL)
  {
      strftime(buf, sizeof(buf), "%d/%m/%y %T ", my_tm);
      fprintf(logfp,"%s ",buf);
      va_start(msg, format);
      vfprintf(logfp, format, msg);
      va_end(msg);
      fclose(logfp);
  }
}

void putdebuglog(char *format, ...)
{
  char    buf[MAXLEN];
  time_t tloc;
  va_list msg;
  FILE *logfp;
  tm *my_tm;

  time(&tloc);
  my_tm = localtime(&tloc);
  strftime(buf, sizeof(buf), "/home/iserv/logs.debug/raw.%d%m%y", my_tm);
  if((logfp = fopen(buf,"a+")) != NULL)
  {
      strftime(buf, sizeof(buf), "%d/%m/%y %T ", my_tm);
      fprintf(logfp,"%s ",buf);
      va_start(msg, format);
      vfprintf(logfp, format, msg);
      va_end(msg);
      fclose(logfp);
  }
}

void cleanup(char *text)
{
  if(ready) {putlog("[Info] Saving channel/user data\n");}
  putlog("[Info] Exiting (%s).\n",text);
  exit(128);
}

void sig_segv(int sig)
{
  sprintf(buf, ":%s QUIT :HELP, I'm on fire! (Core dump)\n",&BOTNAME);writeln(buf);
  // sprintf(buf, ":%s QUIT :I will be right back (Update time)\n",&BOTNAME);writeln(buf);
  sprintf(buf, "SQUIT %s :%s\n",&SERVNAME,&SERVNAME);writeln(buf);
  printf("Exiting (sig_segv)\n");
  exit(-1);
}

void sig_kill(int sig)
{
  sprintf(buf, ":%s QUIT :CTRL-C from shell...\n",&BOTNAME);writeln(buf);
  sprintf(buf, "SQUIT %s :%s\n",&SERVNAME,&SERVNAME);writeln(buf);
  cleanup("sig_kill");
  exit(-1);
}

void sig_hup(int sig)
{
  sprintf(buf, ":%s QUIT :Somebody is hup'ing me. I don't have that function yet, let's die instead :-)\n",&BOTNAME);writeln(buf);
  sprintf(buf, "SQUIT %s :%s\n",&SERVNAME,&SERVNAME);writeln(buf);
  cleanup("sig_hup");
  exit(-1);
}

//void main(int argc, char **argv)
int main()
{
  int i, errflag;
  dumb=1;
  top=NULL;
  ctop=NULL;
  utop=NULL;
  ready=FALSE;
  LASTBACKUP=0;


  signal(SIGINT,sig_kill);
  signal(SIGHUP,SIG_IGN);
  signal(SIGPIPE,SIG_IGN);
  signal(SIGKILL,sig_kill);
  signal(SIGTERM,sig_kill);
  signal(SIGSEGV,sig_segv);
/*
  if(argc==2 && !strcmp(argv[1],"-d"))
	  	rawdebug=TRUE;
*/
	  	rawdebug=TRUE; // DEBUG
  openlog();

  SRVUPTIME=time(NULL);
  LASTSAVE=SRVUPTIME;

  printf("W %s (C) by Jon Suphammer Aka Exion\n",VERSION);
  putlog("[Info] Loading configuration file\n");

  if(!load_config("/home/iserv/etc/w.config"))
  {
     putlog("[Error] Failed to load configuration file\n");
     exit(1);
  }

  // Add a fake user to keep the database in sync
  Srvadduser("$@$","NOT.AVAILABLE","NOWHERE");

  putlog("[Info] Connecting to SQL server\n");
  if(!channels.open())
  {
     putlog("[Error] Failed to connect to channels database\n");
     exit(1);
  }

  if(!nicks.open())
  {
     putlog("[Error] Failed to connect to nicks database\n");
     exit(1);
  }

  //putlog("[Info] Loading channel/user file\n");
  //load_channels();
  //save_channels_sql();
  //exit(0); 

  /* LOGON TO SERVER */
  if(!login_srv((char *)&SERVER))
  {
     putlog("[Error] Failed to connect to server\n");
     exit(1);
  }
  
  idletimer=time(NULL);

  putlog("[Info] Joining channels\n");
  channels.joinChannels();

  putlog("[Info] Enf of Burst\n");
  send_to_server("EOB\n");

  putlog("[Info] Ready...\n");
  ready=TRUE;

// Loop handing input/output
  while(TRUE)
  {
     FD_ZERO(&readfs);
     FD_SET(sckfd,&readfs);
     if(select(FD_SETSIZE, &readfs, NULL, NULL,(dumb ? NULL : &timeout)))
     {
        if(FD_ISSET(sckfd,&readfs))
        {
	   sok = readln();
	   if (sok) spitout();
        }
     }
  }

}

/* EOF */
