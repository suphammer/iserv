//
// W SQL Connection routines
// (C) 1998 by Jon Suphammer
// Converted to use mysql 4.x compliant functions by J.Thomas Stokkeland 2004 (Stoke)
//
// $Id: channelsdb.c 3 2004-11-03 05:12:30Z iserv $
// 

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "global.h"
#include "config.h"
#include <time.h>

boolean channelsclass::open()
{
   results=NULL;

   if (mysql_init(&mysql) == NULL) {
      db_error("Unable to initialize MySQL client structure (chan)\n");
      return False;
   }
   if(mysql_real_connect(&mysql,(char *)&DBHOST,nit_user,nit_pass,(char *)&DBNAME,0,NULL,0) == NULL ) {
      db_error("Unable to connect to SQL server (nick)\n");
      return False;
   }
   return True;
}

boolean channelsclass::close()
{
   if(results!=0) {
      mysql_free_result(results);results=NULL; 
   }
   mysql_close(&mysql);
}

boolean channelsclass::findChannel(char *name, sChannelList *dest)
{
   char myquery[CHANNELLEN+2+255],myname[MAXLEN];
   if(results!=0) {
      mysql_free_result(results);results=NULL;
   }

   mysql_real_escape_string(&mysql,myname,name,strlen(name));

   sprintf(myquery,"select * from channels where name like '%s'",myname);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("findChannel: Error doing query.\n");
      return False;
   }

   results=mysql_store_result(&mysql);
   rowcnt=mysql_num_rows(results);
   currow=mysql_fetch_row(results);
   if(rowcnt==0)
      return False;

   strncpy(dest->name,currow[ct_name],CHANNELLEN);
   strncpy(dest->owner,currow[ct_owner],NICKLEN);
   strncpy(dest->flags,currow[ct_flags],UFLAGSLEN);
   dest->created = atol(currow[ct_created]);
   dest->strictt = atoi(currow[ct_keeptopic]);
   strncpy(dest->topic,currow[ct_topic],TOPICLEN);
   strncpy(dest->onjoin[0],currow[ct_onjoin1],ONJOINLEN);
   strncpy(dest->onjoin[1],currow[ct_onjoin2],ONJOINLEN);
   strncpy(dest->onjoin[2],currow[ct_onjoin3],ONJOINLEN);
   strncpy(dest->onjoin[3],currow[ct_onjoin4],ONJOINLEN);
   strncpy(dest->onjoin[4],currow[ct_onjoin5],ONJOINLEN);
}

boolean channelsclass::joinChannels()
{
   char myquery[CHANNELLEN+2+255];
   int i;

   if(results!=0) {
      mysql_free_result(results);results=NULL;
   }

   sprintf(myquery,"select * from channels");

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("joinChannels: Error doing query.\n");
      return False;
   }

   results=mysql_store_result(&mysql);
   rowcnt=mysql_num_rows(results);
   for(i=0;i<rowcnt;i++)
   {
      currow=mysql_fetch_row(results);

      // Join channel
      //sprintf(myquery,":%s JOIN %s\n",&BOTNAME,currow[ct_name]);send_to_server(myquery);

	sprintf(myquery,":%s SJOIN %ld %s +tn  :@%s\n",&SERVNAME,atol(currow[ct_created]),
		currow[ct_name], &BOTNAME);send_to_server(myquery);

      // Make Channel Service operator
      sprintf(myquery,":%s MODE %s +nto %s\n",&SERVNAME,currow[ct_name],&BOTNAME);send_to_server(myquery);

      // Check if it's a strict topic channel
      if(atoi(currow[ct_keeptopic])==1) {
         sprintf(myquery,":%s TOPIC %s :%s\n",&BOTNAME,currow[ct_name],currow[ct_topic]);send_to_server(myquery);
      }
   }
}

boolean channelsclass::resyncChannels()
{
   char myquery[CHANNELLEN+2+255];
   int i;

   if(results!=0) {
      mysql_free_result(results);results=NULL;
   }

   sprintf(myquery,"select * from channels");

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("joinChannels: Error doing query.\n");
      return False;
   }

   results=mysql_store_result(&mysql);
   rowcnt=mysql_num_rows(results);
   for(i=0;i<rowcnt;i++)
   {
      currow=mysql_fetch_row(results);

      // Make Channel Service operator
      sprintf(myquery,"MODE %s +nto %s\n",currow[ct_name],&BOTNAME);send_to_server(myquery);
   }
}

boolean channelsclass::addChannel(char *name, char *owner)
{
   char myquery[sizeof(channel)+8+255];
   char myname[MAXLEN],myowner[MAXLEN],flags[MAXLEN],topic[MAXLEN];
   char onjoin0[MAXLEN],onjoin1[MAXLEN],onjoin2[MAXLEN],onjoin3[MAXLEN],onjoin4[MAXLEN];

   mysql_real_escape_string(&mysql,myname,name,strlen(name));
   mysql_real_escape_string(&mysql,myowner,owner,strlen(owner));
   mysql_real_escape_string(&mysql,flags,channel.flags,strlen(channel.flags));
   mysql_real_escape_string(&mysql,topic,channel.topic,strlen(channel.topic));
   mysql_real_escape_string(&mysql,onjoin0,channel.onjoin[0],strlen(channel.onjoin[0]));
   mysql_real_escape_string(&mysql,onjoin1,channel.onjoin[1],strlen(channel.onjoin[1]));
   mysql_real_escape_string(&mysql,onjoin2,channel.onjoin[2],strlen(channel.onjoin[2]));
   mysql_real_escape_string(&mysql,onjoin3,channel.onjoin[3],strlen(channel.onjoin[3]));
   mysql_real_escape_string(&mysql,onjoin4,channel.onjoin[4],strlen(channel.onjoin[4]));

   sprintf(myquery,"INSERT INTO channels VALUES ('%s','%s','%s',%ld,%d,'%s','%s','%s','%s','%s','%s')",
           myname,
           myowner,
           flags,
           (long *)time(NULL),
           channel.strictt,
           topic,
           onjoin0,onjoin1,onjoin2,onjoin3,onjoin4
	  );
   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed creating channel.\n");
      return False;
   }
}

boolean channelsclass::addChannelExport(char *name, char *owner)
{
   char myquery[sizeof(channel)+8+255];
   char myname[MAXLEN],myowner[MAXLEN],flags[MAXLEN],topic[MAXLEN];
   char onjoin0[MAXLEN],onjoin1[MAXLEN],onjoin2[MAXLEN],onjoin3[MAXLEN],onjoin4[MAXLEN];

   mysql_real_escape_string(&mysql,myname,name,strlen(name));
   mysql_real_escape_string(&mysql,myowner,owner,strlen(owner));
   mysql_real_escape_string(&mysql,flags,channel.flags,strlen(channel.flags));
   mysql_real_escape_string(&mysql,topic,channel.topic,strlen(channel.topic));
   mysql_real_escape_string(&mysql,onjoin0,channel.onjoin[0],strlen(channel.onjoin[0]));
   mysql_real_escape_string(&mysql,onjoin1,channel.onjoin[1],strlen(channel.onjoin[1]));
   mysql_real_escape_string(&mysql,onjoin2,channel.onjoin[2],strlen(channel.onjoin[2]));
   mysql_real_escape_string(&mysql,onjoin3,channel.onjoin[3],strlen(channel.onjoin[3]));
   mysql_real_escape_string(&mysql,onjoin4,channel.onjoin[4],strlen(channel.onjoin[4]));

   sprintf(myquery,"INSERT INTO channels VALUES ('%s','%s','%s',%ld,%d,'%s','%s','%s','%s','%s','%s')",
           myname,
           myowner,
           flags,
           (long *)time(NULL),
           channel.strictt,
           topic,
           onjoin0,onjoin1,onjoin2,onjoin3,onjoin4
	  );

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed creating channel.\n");
      return False;
   }
}

boolean channelsclass::remChannel(char *name)
{
   char myquery[CHANNELLEN+NICKLEN+4+255];
   char myname[MAXLEN];

   mysql_real_escape_string(&mysql,myname,name,strlen(name));
   sprintf(myquery,"DELETE FROM nicks WHERE channel like '%s'",myname);
   if(mysql_real_query(&mysql,myquery,strlen(myquery)))
   {
      db_error("Failed deleting channel users\n");
      return False;
   }

   sprintf(myquery,"DELETE FROM channels WHERE name like '%s'",myname);
   if(mysql_real_query(&mysql,myquery,strlen(myquery)))
   {
      db_error("Failed deleting channel\n");
      return False;
   }
   return True;
}

boolean channelsclass::setKeepTopic(char *channel, int status)
{
   char myquery[CHANNELLEN+256];
   char mychan[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   sprintf(myquery,"UPDATE channels SET strictt='%d' WHERE name like '%s'", status, mychan);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting channel topic.\n");
      return False;
   }
}

boolean channelsclass::setTopic(char *channel, char *topic)
{
   char myquery[CHANNELLEN+TOPICLEN+256];
   char mychan[MAXLEN],mytopic[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mytopic,topic,strlen(topic));

   sprintf(myquery,"UPDATE channels SET topic='%s' WHERE name like '%s'",mytopic,mychan);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting channel topic.\n");
      return False;
   }
}

boolean channelsclass::setOnJoin(char *channel,int line,char *text)
{
   char myquery[CHANNELLEN+TOPICLEN+256];
   char mychan[MAXLEN],mytext[MAXLEN];

   mysql_real_escape_string(&mysql,mychan,channel,strlen(channel));
   mysql_real_escape_string(&mysql,mytext,text,strlen(text));

   sprintf(myquery,"UPDATE channels SET onjoin%d='%s' WHERE name like '%s'", line, mytext, mychan);

   //printf("onjoin query: %s\n",myquery);

   if(mysql_real_query(&mysql,myquery,strlen(myquery))) {
      db_error("Failed setting channel onjoin.\n");
      return False;
   }
}

void channelsclass::clearProperties()
{
   channel.name[0] = 0;
   channel.flags[0] = 0;
   channel.created = 0;
   channel.strictt = 0;
   channel.topic[0] = 0;
   channel.onjoin[0][0] = 0;
   channel.onjoin[1][0] = 0;
   channel.onjoin[2][0] = 0;
   channel.onjoin[3][0] = 0;
   channel.onjoin[4][0] = 0;
}

