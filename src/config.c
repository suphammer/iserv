/*
 * config.c - w main configuration system
 * (C) 1996-2000 by Jon Suphammer
 *
 */ 

#include "global.h"
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <stdlib.h>

char SERVER[255], BOTNAME[255], IRCNAME[255], HELPFILE[255], CHANFILE[255],
  SERVNAME[255], SERVPASS[255], CSERVICE[255], DBNAME[255], DBHOST[255];

char hostname[64];
int  DEFAULTPORT;
extern u_short IRCPORT;
// ---------------------------------------------
// Function: load_config
// Desc.:    Laster inn data fra w.config
// ---------------------------------------------
int load_config(char *configName)
{
  FILE *fp;
  char line[255];
  char *cmd, *rest;
  const char *line2;
  int i;

  if((fp=fopen(configName,"r"))==NULL)
     return false;

  for (i=0;fgets(line, 255, fp);i++) {
    if(line[0]!='#') {
      if(( rest = (char *)index( line, ' ' )) !=0)
	*( rest ) = '\0'; rest += 1;
	cmd = line;
	*( rest + strlen(rest) -1 ) ='\0';
	if(cmd!=NULL && rest!=NULL) {
	  // printf("CMD: %s, REST: %s\n",cmd, rest);

	  if(!strcmp(cmd,"SERVER"))
	    strcpy(SERVER,rest);
	  
	  if(!strcmp(cmd,"DEFAULTPORT"))
	    {DEFAULTPORT=atoi(rest);IRCPORT=DEFAULTPORT;}

	  if(!strcmp(cmd,"BOTNAME"))
	    strcpy(BOTNAME,rest);  

	  if(!strcmp(cmd,"IRCNAME"))
	    strcpy(IRCNAME,rest);

	  if(!strcmp(cmd,"HELPFILE")) 
	    strcpy(HELPFILE,rest);

	  if(!strcmp(cmd,"CHANFILE"))
	    strcpy(CHANFILE,rest);

	  if(!strcmp(cmd,"SERVNAME"))
	    strcpy(SERVNAME,rest);

	  if(!strcmp(cmd,"SERVPASS"))
	    strcpy(SERVPASS,rest);

	  if(!strcmp(cmd,"CSERVICE"))
	    strcpy(CSERVICE,rest);

          if(!strcmp(cmd,"DBNAME"))
            strcpy(DBNAME,rest);

	  if(!strcmp(cmd,"DBHOST"))
	    strcpy(DBHOST,rest);
	}
    }
  }

  fclose(fp);
  return true;
}

