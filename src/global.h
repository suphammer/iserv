// Global includefile for W
// $Id$

#include <stdio.h>


// enum's

enum boolean {False, True};


// Local includes

#include "struct.h"
#include "proto.h"
#include "db.h"

// Declarations

//char hostname[64];

// Defines

#define SENDSIZE        7
#define SENDDELAY       100000
#define NAME            "CS v1.2.2\337 \251 1996-2007 by Jon Suphammer"
#define VERSION         "v1.2.2\337"
#define SERVDESC        "CS v1.2.2\337 \251 1996-2007 by Jon Suphammer"
#define PREFIX_CHAR     '%'
#define TRUE 1
#define FALSE 0
#define DEBUG		
#define MAXLEN          256

// The else is needed or things might get wrong in if-constructions
#define KILLNEWLINE(q)  if(*(q+strlen(q)-1)=='\n') *(q+strlen(q)-1)='\0'; else {};
#define KILLRETURN(q)   if(*(q+strlen(q)-1)=='\r') *(q+strlen(q)-1)='\0'; else {};
#define STRCASEEQUAL(s1, s2)    (!strcasecmp(s1, s2))
#define null(type) (type) 0L

