//
// W SQL Connection header file
// (C) 1998 by Jon Suphammer
// Converted to use mysql 4.x compliant functions by J.Thomas Stokkeland 2004 (Stoke)
// 
// $Id$
//

// Database
/* This is now defined in config #define dbname "irclink" */
#define nit_user "iserv"
#define nit_pass "mydbpass"

// nicks table
#define nt_channel 0
#define nt_nick 1
#define nt_level 2
#define nt_flags 3
#define nt_seen 4
#define nt_passwd 5
#define nt_authed 6
#define nt_comment 7
#define nt_mask 8
#define nt_authchan 9

// channels table
#define ct_name 0
#define ct_owner 1
#define ct_flags 2
#define ct_created 3
#define ct_keeptopic 4
#define ct_topic 5
#define ct_onjoin1 6
#define ct_onjoin2 7
#define ct_onjoin3 8
#define ct_onjoin4 9
#define ct_onjoin5 10

#include "/usr/include/mysql/mysql.h"

//enum boolean {False, True};

class nicksclass {
   public:
      // Functions
      boolean findUser(char *channel, char *nick, sUserList *dest);
      boolean addUser(char *channel, char *nick);
      boolean addUserExport(char *channel, char *nick);
      boolean remUser(char *channel, char *nick);
      boolean authUser(char *channel, char *nick, sSrvUserList *suser);
      boolean deauthUser(char *channel, char *nick);
      boolean setPasswd(char *channel, char *nick, char *passwd);
      boolean setLevel(char *channel, char *nick, int level);
      boolean setAutoOp(char *channel, char *nick, char *flag);
      boolean setComment(char *channel, char *nick, char *comment);
      boolean setAuthChan(char *channel, char *nick, char *authchan);
      boolean showUsers(char *channel, char *nick);
      boolean showUsersDetail(char *channel, char *nick);
      boolean setSeen(char *channel, char *nick, long seen);
      void clearProperties();
      boolean open();
      boolean close();
      // Properties
      sUserList user;

      int rowcnt;
      MYSQL_ROW currow;
   private:
      MYSQL mysql;
      MYSQL_RES *results;
};

class channelsclass {
   public:
      boolean findChannel(char *channel, sChannelList *dest);
      boolean addChannel(char *channel, char *nick);
      boolean addChannelExport(char *channel, char *nick);
      boolean remChannel(char *channel);
      boolean joinChannels();
      boolean resyncChannels();
      boolean setKeepTopic(char *channel, int status);
      boolean setTopic(char *channel, char *topic);
      boolean setOnJoin(char *channel,int line,char *text);
      void clearProperties();
      boolean open();
      boolean close();
      // Properties
      sChannelList channel;

      int rowcnt;
      MYSQL_ROW currow;
   private:
      MYSQL mysql;
      MYSQL_RES *results;
};

void db_error(char *format, ...);

