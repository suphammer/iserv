/*
 * user.c - user database
 *
 */ 

#include "global.h"
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

extern char buf[512];
sSrvUserList *top;

sSrvUserList* Srvadd_user(sSrvUserList *p, sSrvUserList u)
{
  int i=0;
  //printf("Adduser start....\n");
  if (p == NULL) {
    //printf("Adding user.\n");
    p = (sSrvUserList *) malloc(sizeof(sSrvUserList));
    p->nick = (char *)strdup(u.nick);
    p->host = (char *)strdup(u.host);
    p->server = (char *)strdup(u.server);
    // p->logon =(long *)time(NULL);
    p->logon = time(NULL);
    p->next = NULL;
  }
  else {
    // printf("Place taken, trying next record.\n");
    p->next=Srvadd_user(p->next,u);
  }
  //printf("User added: %d\n",p);
  return p;
}

sSrvUserList *Srvremove_user(sSrvUserList *p, char *nick)
{
  sSrvUserList *candidate;
  
  if (!p) 
    {
      // printf(" The list is empty! \n");
      return 0;
    }
  else
    if (!strcasestr(p->nick,nick)) 
      {
        candidate = p;
        p=p->next;
        // printf("%s is now removed from the list\n",nick);
        free(candidate);
      }
    else p->next=Srvremove_user(p->next, nick);
  return p;
}

void Srvshow_users(sSrvUserList *t)
{
  char ch;
  int a=0;
  
  if (t != NULL)
    {
      printf("List: %s, %s (%s)\n", t->nick, t->host, t->server);
      Srvshow_users(t->next); 
    }
}

sSrvUserList *Srvfind_user(char *nick)
{

  // Chop off first char if nick starts with @.. 
  if (*nick == '@') nick=strtok(nick,"@");

  sSrvUserList *current=NULL;
  sSrvUserList *found=NULL;
  for (current=top; current!=NULL; current=current->next) 
  {
     if ( strcasecmp(current->nick,nick)==0 )
     {
	 found=current;
	 break;
     }
  }
  return found;
}

void Srvshowusers_()
{
   Srvshow_users(top);
}


void Srvbroadcast_(sSrvUserList *t, char *message)
{
  char ch;
  int a=0;
  
  if (t != NULL)
    {
      if (strcmp(t->nick,"$@$"))
      {
        sprintf(buf,":%s NOTICE %s :%s\n",&BOTNAME,t->nick,message);
        writeln(buf);
      }
      Srvbroadcast_(t->next, message);
    }
}

void Srvbroadcast(char *message)
{
  Srvbroadcast_(top, message);
}

void Srvwho_(sSrvUserList *t, char *nick)
{
  char ch;
  int a=0;
  
  if (t != NULL)
    {
      if (strcmp(t->nick,"$@$"))
      {
	send_to_user(nick, "%-15s %ld %-25s %s\n",
		t->nick, t->logon, t->server, t->host);
      }
      Srvwho_(t->next, nick);
    }
}

void Srvwho(char *nick)
{
	send_to_user(nick, "\002Nick            Login      Server                    Host\002\n");
	Srvwho_(top, nick);
}

void Srvadduser(char *nick, char *host, char *server)
{
   sSrvUserList cuser;
   cuser.nick=strdup(nick);
   cuser.host=strdup(host);
   cuser.server=strdup(server);
   top=Srvadd_user(top,cuser);
}

void Srvremuser(char *nick)
{
   Srvremove_user(top,nick);
}

void Srvchuser(char *onick, char *nick)
{
   sSrvUserList *tmpuser;
   if(tmpuser=Srvfind_user(onick))
      strcpy(tmpuser->nick,nick);
   else
      return;
}

char *Srvgetuserhost(char *nick)
{
   sSrvUserList *tmpuser;
   if(tmpuser=Srvfind_user(nick)) 
      return tmpuser->host;
   else
      return NULL;
}

